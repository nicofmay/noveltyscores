import dendropy
import dendropy
import sys
import time
import os
import argparse



parser = argparse.ArgumentParser(description='Calculate Phylogenetic Novelty Scores or Effective Number of Sequences for a given tree, or simulate trees and calculate the scores for them.')
parser.add_argument("-o",'--output',default="", help='Output file where to store results.')
parser.add_argument("-t",'--tree',default="", help='the newick-format tree for which to run the methods.')
args = parser.parse_args()

oFile=open(args.output,"w")





#weighting scheme from Gerstin, Sonnhammer and Chothia, 1994.
def GSCweights(tree):
	for node in tree.postorder_node_iter():
		if node.is_leaf():
			node.final=node.edge_length
			node.nList=[node]
			if node.final<0.000001:
				node.final=0.000001
			if not (type(node.final) is float):
				exit()

		children=node.child_nodes()
		if len(children)==2 and (type(node.edge_length) is float):
			node.nList=children[0].nList+children[1].nList
			
			sum1=0.0
			for node2 in node.nList:
				sum1+=node2.final
			for node2 in node.nList:
				node2.final=node2.final + node.edge_length*(node2.final/sum1)
	scores=[]
	names=[]
	for node in tree.postorder_node_iter():
		if node.label==None:
			node.label=""
		if node.is_leaf():
			names.append(node.taxon.label)
			scores.append(node.final)
	
	return names,scores


#print args.tree
tree=dendropy.Tree.get_from_string(args.tree,schema="newick")
results=GSCweights(tree)
print results
for i in range(len(results[0])):
	oFile.write(results[0][i]+"   "+str(results[1][i])+"\n")
oFile.close()
exit()







