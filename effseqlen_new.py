import dendropy
from dendropy import treesim
import sys
import math
from operator import mul
import time
import os

import argparse
import numpy as np
from scipy.linalg import expm
import json
#import scipy.special as special
import scipy.stats as stats


#example usage: 
#python /Users/demaio/Desktop/PhyloNe/effseqlen_new.py -t "((L1:0.1,L2:0.1):0.2,L3:0.3);" -c '{"L1":0,"L2":1,"L3":-1}' -s -S -n 2 -l 5 -b 0.5 -m GTR -k 2.5 -r 0.1 0.2 0.3 0.4 0.5 0.6 -f 0.2 0.3 0.3 0.2
#this both runs 2 simulated trees and analyzes one user-specified input tree
#python /Users/demaio/Desktop/PhyloNe/effseqlen_new.py -s -S -l 4 -n 100 -m HKY -k 3.0 --simulateFreq --fastMethods -o /Users/demaio/Desktop/PhyloNe/simulations_100reps_4leaves.txt
#python /Users/demaio/Desktop/PhyloNe/effseqlen_new.py -s -S -l 3 -n 2 -m HKY -k 3.0 --simulateFreq --noSlowMethods -o /Users/demaio/Desktop/PhyloNe/simulations_2reps_4leaves_scale01.txt --scale 0.1

#((((((((((Human:0.009887, Chimp:0.003467):0.001881, Bonobo:0.002643):0.005314, Gorilla:0.008931):0.009256, Orangutan:0.018898):0.003421, Gibbon:0.023093):0.011557, ((((Rhesus:0.003408, Crab_eating_macaque:0.00236):0.005353, Baboon:0.00851):0.004081, Green_monkey:0.012951):0.005944, (Proboscis_monkey:0.006916, Golden_snub-nosed_monkey:0.006479):0.011902):0.021607):0.021754, (Marmoset:0.035258, Squirrel_monkey:0.032763):0.03735):0.061217, Tarsier:0.142005):0.011885, (Mouse_lemur:0.092909, Bushbaby:0.128695):0.034263):0.016622, Tree_shrew:0.184214);

#((((((((((((((((((Human:0.00655, Chimp:0.00684):0.00422, Gorilla:0.008964):0.009693, Orangutan:0.01894):0.003471, Gibbon:0.02227):0.01204, (((Rhesus:0.004991, Crab_eating_macaque:0.004991):0.003, Baboon:0.008042):0.01061, Green_monkey:0.027):0.025):0.02183, (Marmoset:0.03, Squirrel_monkey:0.01035):0.01965):0.07261, Bushbaby:0.13992):0.013494, Chinese_tree_shrew:0.174937):0.002, (((Squirrel:0.125468, (Lesser_Egyptian_jerboa:0.1, ((Prairie_vole:0.08, (Chinese_hamster:0.04, Golden_hamster:0.04):0.04):0.06, (Mouse:0.084509, Rat:0.091589):0.047773):0.06015):0.1):0.022992, (Naked_mole_rat:0.1, (Guinea_pig:0.065629, (Chinchilla:0.06,  Brush_tailed_rat:0.1):0.06):0.05):0.06015):0.025746, (Rabbit:0.114227, Pika:0.201069):0.101463):0.015313):0.020593, (((Pig:0.12, ((Alpaca:0.047275, Wild_bactrian_camel:0.04):0.04, ((Dolphin:0.034688, Killer_whale:0.039688):0.03, (Tibetan_antelope:0.1, (Cow:0.1, (Sheep:0.05, Domestic_goat:0.05):0.05):0.01):0.013592):0.025153):0.020335):0.02, (((Horse:0.059397, White_rhinoceros:0.025):0.05, (Cat:0.098612, (Dog:0.052458, (Ferret:0.05, (Panda:0.02, (Pacific_walrus:0.02, Weddell_seal:0.02):0.02):0.03):0.03):0.02):0.049845):0.006219, ((Black_flying_fox:0.05, Megabat:0.063399):0.05, (Big_brown_bat:0.02, (Davids_myotis:0.04, Microbat:0.04254):0.05):0.06):0.033706):0.004508):0.011671, (Hedgehog:0.221785, (Shrew:0.169562, Star-nosed_mole:0.1):0.1):0.056393):0.021227):0.023664, (((((Elephant:0.002242, Cape_elephant_shrew:0.05):0.04699, Manatee:0.1):0.049697, (Cape_golden_mole:0.03, Tenrec:0.235936):0.01):0.03, Aardvark:0.03):0.02, Armadillo:0.169809):0.006717):0.234728, (Opossum:0.125686, (Tasmanian_devil:0.1, Wallaby:0.072008):0.05):0.2151):0.071664, Platypus:0.456592):0.109504, (((((Rock_pigeon:0.1, ((Saker_falcon:0.1, Peregrine_falcon:0.1):0.03, (((Collared_flycatcher:0.04, ((White_throated_sparrow:0.034457, Medium_ground_finch:0.041261):0.015, Zebra_finch:0.06):0.01):0.052066, Tibetan_ground_jay:0.06):0.025, (Budgerigar:0.046985, (Puerto_Rican_parrot:0.026, Scarlet_macaw:0.026):0.01):0.04):0.064703):0.06):0.05, (Mallard_duck:0.1, (Chicken:0.041254, Turkey:0.085718):0.031045):0.09):0.22, American_alligator:0.25):0.045143, ((Green_seaturtle:0.1, Painted_turtle:0.1):0.05, (Chinese_softshell_turtle:0.1, Spiny_softshell_turtle:0.1):0.05):0.04):0.01, Lizard:0.447):0.122):0.05, Frog_X._tropicalis:0.977944):0.1, Coelacanth:0.977944):0.111354, (((((((Tetraodon:0.124159, (Fugu:0.103847, Yellowbelly_pufferfish:0.1):0.1):0.09759, (Nile_tilapia:0.1, (Princess_of_Burundi:0.05, (Burtons_mouthbreeder:0.05, (Zebra_mbuna:0.05, Pundamilia_nyererei:0.05):0.05):0.05):0.1):0.1):0.09759, (Medaka:0.38197, Southern_platyfish:0.4):0.1):0.015, Stickleback:0.246413):0.045, Atlantic_cod:0.25):0.22564, (Zebrafish:0.430752, Mexican_tetra:0.4):0.3):0.143632, Spotted_gar:0.4):0.326688):0.2, Lamprey:0.975747);



		
		
		
		
AAs = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']		
#Le and Gascuel 2008 rate matrix
LGrates=[[0,0.425093,0.276818,0.395144,2.489084,0.969894,1.038545,2.06604,0.358858,0.14983,0.395337,0.536518,1.124035,0.253701,1.177651,4.727182,2.139501,0.180717,0.218959,2.54787],
	[0.425093,0,0.751878,0.123954,0.534551,2.807908,0.36397,0.390192,2.426601,0.126991,0.301848,6.326067,0.484133,0.052722,0.332533,0.858151,0.578987,0.593607,0.31444,0.170887],
	[0.276818,0.751878,0,5.076149,0.528768,1.695752,0.541712,1.437645,4.509238,0.191503,0.068427,2.145078,0.371004,0.089525,0.161787,4.008358,2.000679,0.045376,0.612025,0.083688],
	[0.395144,0.123954,5.076149,0,0.062556,0.523386,5.24387,0.844926,0.927114,0.01069,0.015076,0.282959,0.025548,0.017416,0.394456,1.240275,0.42586,0.02989,0.135107,0.037967],
	[2.489084,0.534551,0.528768,0.062556,0,0.084808,0.003499,0.569265,0.640543,0.320627,0.594007,0.013266,0.89368,1.105251,0.075382,2.784478,1.14348,0.670128,1.165532,1.959291],
	[0.969894,2.807908,1.695752,0.523386,0.084808,0,4.128591,0.267959,4.813505,0.072854,0.582457,3.234294,1.672569,0.035855,0.624294,1.223828,1.080136,0.236199,0.257336,0.210332],
	[1.038545,0.36397,0.541712,5.24387,0.003499,4.128591,0,0.348847,0.423881,0.044265,0.069673,1.807177,0.173735,0.018811,0.419409,0.611973,0.604545,0.077852,0.120037,0.245034],
	[2.06604,0.390192,1.437645,0.844926,0.569265,0.267959,0.348847,0,0.311484,0.008705,0.044261,0.296636,0.139538,0.089586,0.196961,1.73999,0.129836,0.268491,0.054679,0.076701],
	[0.358858,2.426601,4.509238,0.927114,0.640543,4.813505,0.423881,0.311484,0,0.108882,0.366317,0.697264,0.442472,0.682139,0.508851,0.990012,0.584262,0.597054,5.306834,0.119013],
	[0.14983,0.126991,0.191503,0.01069,0.320627,0.072854,0.044265,0.008705,0.108882,0,4.145067,0.159069,4.273607,1.112727,0.078281,0.064105,1.033739,0.11166,0.232523,10.649107],
	[0.395337,0.301848,0.068427,0.015076,0.594007,0.582457,0.069673,0.044261,0.366317,4.145067,0,0.1375,6.312358,2.592692,0.24906,0.182287,0.302936,0.619632,0.299648,1.702745],
	[0.536518,6.326067,2.145078,0.282959,0.013266,3.234294,1.807177,0.296636,0.697264,0.159069,0.1375,0,0.656604,0.023918,0.390322,0.748683,1.136863,0.049906,0.131932,0.185202],
	[1.124035,0.484133,0.371004,0.025548,0.89368,1.672569,0.173735,0.139538,0.442472,4.273607,6.312358,0.656604,0,1.798853,0.099849,0.34696,2.020366,0.696175,0.481306,1.898718],
	[0.253701,0.052722,0.089525,0.017416,1.105251,0.035855,0.018811,0.089586,0.682139,1.112727,2.592692,0.023918,1.798853,0,0.094464,0.361819,0.165001,2.457121,7.803902,0.654683],
	[1.177651,0.332533,0.161787,0.394456,0.075382,0.624294,0.419409,0.196961,0.508851,0.078281,0.24906,0.390322,0.099849,0.094464,0,1.338132,0.571468,0.095131,0.089613,0.296501],
	[4.727182,0.858151,4.008358,1.240275,2.784478,1.223828,0.611973,1.73999,0.990012,0.064105,0.182287,0.748683,0.34696,0.361819,1.338132,0,6.472279,0.248862,0.400547,0.098369],
	[2.139501,0.578987,2.000679,0.42586,1.14348,1.080136,0.604545,0.129836,0.584262,1.033739,0.302936,1.136863,2.020366,0.165001,0.571468,6.472279,0,0.140825,0.245841,2.188158],
	[0.180717,0.593607,0.045376,0.02989,0.670128,0.236199,0.077852,0.268491,0.597054,0.11166,0.619632,0.049906,0.696175,2.457121,0.095131,0.248862,0.140825,0,3.151815,0.18951],
	[0.218959,0.31444,0.612025,0.135107,1.165532,0.257336,0.120037,0.054679,5.306834,0.232523,0.299648,0.131932,0.481306,7.803902,0.089613,0.400547,0.245841,3.151815,0,0.249313],
	[2.54787,0.170887,0.083688,0.037967,1.959291,0.210332,0.245034,0.076701,0.119013,10.649107,1.702745,0.185202,1.898718,0.654683,0.296501,0.098369,2.188158,0.18951,0.249313,0]]

AAbaseFreqs=[0.079066,0.055941,0.041977,0.053052,0.012937,0.040767,0.071586,0.057337,0.022355,0.062157,0.099081,0.0646,0.022951,0.042302,0.04404,0.061197,0.053287,0.012066,0.034155,0.069147]

freqs=[AAbaseFreqs]
s=sum(freqs[0])
if s!=1.0:
	for i in range(4):
		freqs[0][i]=freqs[0][i]/s
	
pi=[AAbaseFreqs]
s2=sum(pi[0])
if s2!=1.0:
	for i in range(4):
		pi[0][i]=pi[0][i]/s2
		
modelType="LG"






#Q matrix for LG
def LGqM(F):
	qMatrix2=[]
	totRate=0.0
	for i in range(20):
		qMatrix2.append([])
		for j in range(20):
			if i==j:
				qMatrix2[i].append(0.0)
			else:
				qMatrix2[i].append(LGrates[i][j]*F[j])
				totRate+=qMatrix2[i][j]*F[i]
	for i in range(20):
		for j in range(20):
			qMatrix2[i][j]=qMatrix2[i][j]/totRate
	
	for i in range(20):
		qMatrix2[i][i]=-sum(qMatrix2[i])
	return qMatrix2
	



#if modelType=="GTR":
#	model = dendropy.model.discrete.NucleotideCharacterEvolutionModel(base_freqs=[0.25,0.25,0.25,0.25], state_alphabet=None, rng=None)
if modelType=="JC":
	model = dendropy.model.discrete.Jc69(state_alphabet=None, rng=None)
	qMatrix=[model.qmatrix(rate=1.0)]
	freqs[0]=[0.25,0.25,0.25,0.25]
	pi[0]=[0.25,0.25,0.25,0.25]
elif modelType=="HKY":
	model = dendropy.model.discrete.Hky85(kappa=args.kappa, base_freqs=freqs[0], state_alphabet=None, rng=None)
	qMatrix=[model.qmatrix(rate=1.0)]
elif modelType=="GTR":
	#rates of a GTR model: these are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C.
	# the order of the rates is AC, AG, AT, CG, CT, GT.
	rates=args.rates
	qMatrix2=[[0.0,rates[0]*freqs[0][1],rates[1]*freqs[0][2],rates[2]*freqs[0][3]], [rates[0]*freqs[0][0],0.0,rates[3]*freqs[0][2],rates[4]*freqs[0][3]], [rates[1]*freqs[0][0],rates[3]*freqs[0][1],0.0,rates[5]*freqs[0][3]], [rates[2]*freqs[0][0],rates[4]*freqs[0][1],rates[5]*freqs[0][2],0.0]]
	qMatrix2[0][0]=-(rates[0]*freqs[0][1]+rates[1]*freqs[0][2]+rates[2]*freqs[0][3])
	qMatrix2[1][1]=-(rates[0]*freqs[0][0]+rates[3]*freqs[0][2]+rates[4]*freqs[0][3])
	qMatrix2[2][2]=-(rates[1]*freqs[0][0]+rates[3]*freqs[0][1]+rates[5]*freqs[0][3])
	qMatrix2[3][3]=-(rates[2]*freqs[0][0]+rates[4]*freqs[0][1]+rates[5]*freqs[0][2])
	totRate=-freqs[0][0]*qMatrix2[0][0]-freqs[0][1]*qMatrix2[1][1]-freqs[0][2]*qMatrix2[2][2]-freqs[0][3]*qMatrix2[3][3]
	for i in range(4):
		for j in range(4):
			qMatrix2[i][j]=qMatrix2[i][j]/totRate
	qMatrix=[qMatrix2]
elif modelType=="LG":
	AAbaseFreqs=[0.079066,0.055941,0.041977,0.053052,0.012937,0.040767,0.071586,0.057337,0.022355,0.062157,0.099081,0.0646,0.022951,0.042302,0.04404,0.061197,0.053287,0.012066,0.034155,0.069147]

	AApi=AAbaseFreqs
	pi=[AApi]
	s2=sum(pi[0])
	if s2!=1.0:
		for i in range(20):
			pi[0][i]=pi[0][i]/s2

	AAfreqs=AAbaseFreqs
	freqs=[AAfreqs]
	s=sum(freqs[0])
	if s!=1.0:
		for i in range(20):
			freqs[0][i]=freqs[0][i]/s
	qMatrix=[LGqM(freqs[0])]
	
	
	
	
	
#Q matrix for nuc models
def nucqM(F,modelType="JC",k=1.0, rates=[0.1,0.4,0.15,0.09,0.42,0.13]):
	if modelType=="JC":
		model = dendropy.model.discrete.Jc69(state_alphabet=None, rng=None)
		qMatrix=[model.qmatrix(rate=1.0)]
	elif modelType=="HKY":
		model = dendropy.model.discrete.Hky85(kappa=k, base_freqs=F, state_alphabet=None, rng=None)
		qMatrix=[model.qmatrix(rate=1.0)]
	elif modelType=="GTR":
		#rates of a GTR model: these are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C.
		# the order of the rates is AC, AG, AT, CG, CT, GT.
		freqs=[F]
		qMatrix2=[[0.0,rates[0]*freqs[0][1],rates[1]*freqs[0][2],rates[2]*freqs[0][3]], [rates[0]*freqs[0][0],0.0,rates[3]*freqs[0][2],rates[4]*freqs[0][3]], [rates[1]*freqs[0][0],rates[3]*freqs[0][1],0.0,rates[5]*freqs[0][3]], [rates[2]*freqs[0][0],rates[4]*freqs[0][1],rates[5]*freqs[0][2],0.0]]
		qMatrix2[0][0]=-(rates[0]*freqs[0][1]+rates[1]*freqs[0][2]+rates[2]*freqs[0][3])
		qMatrix2[1][1]=-(rates[0]*freqs[0][0]+rates[3]*freqs[0][2]+rates[4]*freqs[0][3])
		qMatrix2[2][2]=-(rates[1]*freqs[0][0]+rates[3]*freqs[0][1]+rates[5]*freqs[0][3])
		qMatrix2[3][3]=-(rates[2]*freqs[0][0]+rates[4]*freqs[0][1]+rates[5]*freqs[0][2])
		totRate=-freqs[0][0]*qMatrix2[0][0]-freqs[0][1]*qMatrix2[1][1]-freqs[0][2]*qMatrix2[2][2]-freqs[0][3]*qMatrix2[3][3]
		for i in range(4):
			for j in range(4):
				qMatrix2[i][j]=qMatrix2[i][j]/totRate
		qMatrix=[qMatrix2]
		#print qMatrix
		print("GTR")
	return qMatrix[0]






#probability of any mutation occurring on this branch from each nucleotide
def mutProbsNew(bl, F, pos=0, kappa=1.0, qM=[], modelType="JC"):
	#print("kappa: "+str(kappa))
	if modelType=="JC":
		return [1.0-math.exp(-bl)]*4
	elif modelType=="HKY":
			mutProbs=[]
			model = dendropy.model.discrete.Hky85(kappa=kappa, base_freqs=F, state_alphabet=None, rng=None)
			qMatrix3=model.qmatrix(rate=1.0)
			for i in range(4):
				mutProbs.append(1.0-math.exp(qMatrix3[i][i]*bl))
				#mutProbs.append(1.0-math.exp(qM[i][i]*bl))
			return mutProbs
			
	elif modelType=="GTR":
			#rates of a GTR model: these are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C.
			# the order of the rates is AC, AG, AT, CG, CT, GT.
			mutProbs=[]
			for i in range(4):
				mutProbs.append(1.0-math.exp(qM[i][i]*bl))
			#rates=args.rates
			#mutProbs.append(1.0-math.exp(-(rates[0]*F[1]+rates[1]*F[2]+rates[2]*F[3])*bl))
			#mutProbs.append(1.0-math.exp(-(rates[0]*F[0]+rates[3]*F[2]+rates[4]*F[3])*bl))
			#mutProbs.append(1.0-math.exp(-(rates[1]*F[0]+rates[3]*F[1]+rates[5]*F[3])*bl))
			#mutProbs.append(1.0-math.exp(-(rates[2]*F[0]+rates[4]*F[1]+rates[5]*F[2])*bl))
			print("GTR model")
			return mutProbs
	elif modelType=="LG":
			#rates of a LG model: these are symmetrical, so the first is both r_AR and r_RA. For the Q matrix you have q_AC = r_AR * pi_R.
			mutProbs=[]
			for i in range(20):
				#totRate=qM[i][i]
				#for j in range(20):
				#	if i!=j:
				#		totRate+=qM[i][j]
				totRate=qM[i][i]*bl
				mutProbs.append(1.0-math.exp(totRate))
			#print "LG model"
			return mutProbs

#probability of any mutation occurring on this branch 
def mutProbs(bl, F, pos=0, kappa=1.0, qM=[], modelType="JC"):
	#print("kappa: "+str(kappa))
	if modelType=="JC":
		newM=np.ones((4, 4))
		stay=0.25+0.75*math.exp(-bl*4/3.0)
		leave=0.25-0.25*math.exp(-bl*4/3.0)
		for i in range(4):
			for j in range(4):
				if i==j:
					newM[i][j]=stay
				else:
					newM[i][j]=leave
		return newM
	elif modelType=="HKY":
		model = dendropy.model.discrete.Hky85(kappa=kappa, base_freqs=F, state_alphabet=None, rng=None)
		newM=model.pmatrix(bl, rate=1.0)
		#print "newM"
		#print newM
		return newM
	elif modelType=="GTR":
		# rates=args.rates
# 		freqs2=F   
# 		qMatrix3=[[0.0,rates[0]*freqs2[1],rates[1]*freqs2[2],rates[2]*freqs2[3]], [rates[0]*freqs2[0],0.0,rates[3]*freqs2[2],rates[4]*freqs2[3]], [rates[1]*freqs2[0],rates[3]*freqs2[1],0.0,rates[5]*freqs2[3]], [rates[2]*freqs2[0],rates[4]*freqs2[1],rates[5]*freqs2[2],0.0]]
# 		qMatrix3[0][0]=-(rates[0]*freqs2[1]+rates[1]*freqs2[2]+rates[2]*freqs2[3])
# 		qMatrix3[1][1]=-(rates[0]*freqs2[0]+rates[3]*freqs2[2]+rates[4]*freqs2[3])
# 		qMatrix3[2][2]=-(rates[1]*freqs2[0]+rates[3]*freqs2[1]+rates[5]*freqs2[3])
# 		qMatrix3[3][3]=-(rates[2]*freqs2[0]+rates[4]*freqs2[1]+rates[5]*freqs2[2])
# 		totRate=-freqs2[0]*qMatrix3[0][0]-freqs2[1]*qMatrix3[1][1]-freqs2[2]*qMatrix3[2][2]-freqs2[3]*qMatrix3[3][3]
		qMatrix3=[[0.0]*4]*4
		for i in range(4):
			for j in range(4):
				qMatrix3[i][j]=qMatrix3[i][j]*bl #/totRate
		print("GTR model")
		return expm(qMatrix3)
	elif modelType=="LG":
		qM2=[]
		for i in range(20):
			qM2.append([])
			for j in range(20):
				qM2[i].append(qM[i][j]*bl)
		#print "LG model"
		return expm(qM2)

		
#allow AAs		
#simulate alignment column, and record the nucleotides at node.Seq entries
def simulateSeq(tree, append=False, pos=0, modelType="JC", k=1.0):
	root=tree.seed_node
	elements=[]
	if modelType=="LG":
		nResidues=20
		qMat=LGqM(pi[pos])
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
		qMat=nucqM(pi[pos],modelType=modelType,k=k, rates=[0.1,0.4,0.15,0.09,0.42,0.13])
	for i in range(nResidues):
		elements.append(i)
	#elements = [0,1,2,3]
	rootNuc=np.random.choice(elements, 1, p=pi[pos])[0]
	if root.is_leaf():
		if append:
			root.Seq.append(rootNuc)
		else:
			root.Seq=rootNuc
		return
	children=root.child_nodes()
	for c in children:
		simulateSeqNodes(c,rootNuc,append=append, pos=pos, elements=elements, qM=qMat, modelType=modelType)
		
		
		
def simulateSeqNodes(c,rootNuc,append=False, pos=0, elements=[0,1,2,3], qM=[], modelType="JC"):
	if modelType=="HKY" or modelType=="JC":
		probs=mutProbs(c.edge_length, pi[pos], pos=pos, qM=None, modelType=modelType)
	else:
		probs=mutProbs(c.edge_length, pi[pos], pos=pos, qM=qM, modelType=modelType)
	#print "probs"
	#print probs
	newProbs=[]
	for i in range(len(elements)):
		if probs[rootNuc][i]<0.0:
			newProbs.append(0.0)
		else:
			newProbs.append(probs[rootNuc][i])
	sum1=sum(newProbs)
	for i in range(len(elements)):
		newProbs[i]=newProbs[i]/sum1
	#print newProbs
	newNuc=np.random.choice(elements, 1, p=newProbs)[0]
	if c.is_leaf():
		if append:
			c.Seq.append(newNuc)
		else:
			c.Seq=newNuc
		return
	children=c.child_nodes()
	for c2 in children:
		simulateSeqNodes(c2,newNuc,append=append, pos=pos, elements=elements, qM=qM, modelType=modelType)




def eucDistance(scores,oldScores):
	sum=0.0
	for i in range(len(scores)):
		sum+=(scores[i]-oldScores[i])**2
	return math.sqrt(sum)




#Calculate tree and leaf scores by simulating nucleotides at internal nodes, consistent with observed data
def calculateScoreSimulationData(tree, step=100, eps=0.01, fast=2, F=[0.25,0.25,0.25,0.25], k=1.0, modelType="JC"):
	elements=[]
	if modelType=="LG":
		nResidues=20
		qMat=LGqM(F)
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
		if modelType=="GTR":
			qMat=nucqM(F,modelType=modelType,k=k)
	for i in range(nResidues):
		elements.append(i)
	leaves=tree.leaf_nodes()
	totScores=[0.0]*len(leaves)
	scores=[0.0]*len(leaves)
	oldScores=[-1.0]*len(leaves)
	nSteps=0
	if fast==2:
		fast=1
		for node in leaves:
			if node.Seq!=-1:
				fast=0
	for node in tree.postorder_node_iter():
		if node!=tree.seed_node:
			if modelType=="HKY" or modelType=="JC":
				node.pMatrix=mutProbs(node.edge_length, F, kappa=k, qM=None, modelType=modelType)
				node.probMuts=mutProbsNew(node.edge_length, F, kappa=k, qM=None, modelType=modelType)
			else:
				node.pMatrix=mutProbs(node.edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				node.probMuts=mutProbsNew(node.edge_length, F, kappa=k, qM=qMat, modelType=modelType)
	while eucDistance(scores,oldScores)>eps:
		for j in range(len(scores)):
			oldScores[j]=scores[j]
		for i in range(step):
			nSteps+=1
			if fast==1:
				simulateSeqDataFast(tree, elements=elements)
			else:
				simulateSeqData(tree, elements=elements)
			#newScores=[]
			#for node in leaves:
			#	newScores.append(node.score)
			for j in range(len(scores)):
				#totScores[j]+=newScores[j]
				totScores[j]+=leaves[j].score
		for j in range(len(scores)):
			scores[j]=totScores[j]/nSteps
	print("steps: "+str(nSteps))
		#print oldScores
		#print scores
		
	names=[]
	for node in leaves:
		names.append(node.taxon.label.replace(" ","_"))
	
	return names,scores
	#return scores
			
	


def simulateSeqData(tree, elements=[0,1,2,3]):
	nResidues=len(elements)
	newElements=[]
	for i in range(nResidues):
		newElements.append(i)
	newElements.append(nResidues)
	for node in tree.postorder_node_iter():
		if node.is_leaf():
			if node.Seq==-1:
				node.LK=[]
				for i in range(nResidues):
					node.LK.append(1.0)
			else:
				node.LK=[]
				for i in range(nResidues):
					node.LK.append(0.0)
				node.LK[node.Seq]=1.0
			continue
		children=node.child_nodes()
		assert len(children)==2
		pMatrix1=children[0].pMatrix
		pMatrix2=children[1].pMatrix
			
		#likelihood and score up the branch
		node.LK=[]
		children[0].LKup=[]
		children[1].LKup=[]
		for i in range(nResidues):
			sub1=0.0
			sub2=0.0
			for j in range(nResidues):
				sub1+=pMatrix1[i][j]*children[0].LK[j]
				sub2+=pMatrix2[i][j]*children[1].LK[j]
			children[0].LKup.append(sub1)
			children[1].LKup.append(sub2)
			node.LK.append(sub1*sub2)
	finalLK=0.0
	for i in range(nResidues):
		finalLK+=pi[0][i]*tree.seed_node.LK[i]
	rootCondProbs=[]
	for i in range(nResidues):
		rootCondProbs.append(pi[0][i]*tree.seed_node.LK[i]/finalLK)
	
	root=tree.seed_node
	#elements = [0,1,2,3]
	rootNuc=np.random.choice(elements, 1, p=rootCondProbs)[0]
	#print rootNuc
	components=[[]]
	root.component=0
	if root.is_leaf():
	#	root.Seq=rootNuc
		return	1, [1.0]
	children=root.child_nodes()
	#for c in children:
	#print("children")
	#print(children)
	simulateSeqNodesData(children,rootNuc, components, newElements=newElements, nResidues=nResidues)
	components={}
	for node in tree.leaf_nodes():
		if node.component in components.keys():
			components[node.component]+=1
		else:
			components[node.component]=1
	#scores=[]
	for node in tree.leaf_nodes():
		node.score=1.0/components[node.component]
		#scores.append(1.0/components[node.component])
	#for i in range(len(scores)):
	#	totScore+=scores[i]
	#return scores
	
		
def simulateSeqNodesData(children,rootNuc,components, newElements=[0,1,2,3,4], nResidues=4):
	#nResidues=len(elements)
	for c in children:
		probs=c.pMatrix
		newProbs=[]
		cTotLK=0.0
		for i in range(nResidues):
			cTotLK+=c.LK[i]*probs[rootNuc][i]
		for i in range(nResidues):
			newProbs.append(probs[rootNuc][i]*c.LK[i]/cTotLK)
		#print(c.edge_length)
		probsMut=c.probMuts
		#probsMut=mutProbsNew(c.edge_length)
		#print probsMut
		newProbs.append((1.0-probsMut[rootNuc])*c.LK[rootNuc]/cTotLK)
		newProbs[rootNuc]-=(1.0-probsMut[rootNuc])*c.LK[rootNuc]/cTotLK
		#elements = [0,1,2,3,4]
		for i in range(len(newProbs)):
			if newProbs[i]<0.0:
				newProbs[i]=0.0
		newNuc=np.random.choice(newElements, 1, p=newProbs)[0]
		#print newProbs
		#print newNuc
		if newNuc==nResidues:
			c.component=c.parent_node.component
			if not c.is_leaf():
				children=c.child_nodes()
				simulateSeqNodesData(children,rootNuc,components, newElements=newElements, nResidues=nResidues)
		else:
			c.component=len(components)
			components.append([])
			if not c.is_leaf():
				children=c.child_nodes()
				simulateSeqNodesData(children,newNuc,components, newElements=newElements, nResidues=nResidues)
		
		



def simulateSeqDataFast(tree, elements = [0,1,2,3]):
	nResidues=len(elements)
	newElements=[]
	for i in range(nResidues):
		newElements.append(i)
	newElements.append(nResidues)
	root=tree.seed_node
	#elements = [0,1,2,3]
	rootNuc=np.random.choice(elements, 1, p=pi[0])[0]
	root.simNuc=rootNuc
	counts=[]
	countsCounts=[]
	for i in range(nResidues):
		counts.append(0)
		countsCounts.append([])
	counts[rootNuc]=1
	#countsCounts=[[],[],[],[]]
	countsCounts[rootNuc].append(0)
	for node in tree.preorder_node_iter():
		if node==root:
			continue
		rootNuc=node.parent_node.simNuc%nResidues
		probs=[]
		for i in range(nResidues):
			probs.append(node.pMatrix[rootNuc][i])
		if node.edge_length<0.000001:
			newNuc=nResidues
		else:
			#elements = [0,1,2,3,4]
			prob=(1.0-node.probMuts[rootNuc])
			if prob>probs[rootNuc]:
				probs.append(probs[rootNuc])
				probs[rootNuc]=0.0
			else:
				probs.append(prob)
				probs[rootNuc]-=prob
			sum1=sum(probs)
			if sum1>1.0000000001 or sum1<0.9999999999:
				for i in range(nResidues+1):
					probs[i]=probs[i]/sum1
			newNuc=np.random.choice(newElements, 1, p=probs)[0]
		if newNuc==nResidues:
			if node.is_leaf():
				countsCounts[rootNuc][int(node.parent_node.simNuc/nResidues)]+=1
			node.simNuc=node.parent_node.simNuc
		else:
			if node.is_leaf():
				countsCounts[newNuc].append(1)
			else:
				countsCounts[newNuc].append(0)
			node.simNuc=counts[newNuc]*nResidues + newNuc
			counts[newNuc]+=1
	#print counts
	#print countsCounts
	for node in tree.leaf_nodes():
		#print node.simNuc
		node.score=1.0/countsCounts[node.simNuc%nResidues][int(node.simNuc/nResidues)]

	

				
					
		
		




#New DP algorithm accounting for general substitution models and ancestral nucleotides
def effSeqLenData(tree, pos=0, modelType="JC"):
	elements=[]
	if modelType=="LG":
		nResidues=20
		qMat=LGqM(pi[pos])
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
		if modelType=="GTR":
			qMat=nucqM(pi[pos],modelType=modelType,k=1.0)
	for i in range(nResidues):
		elements.append(i)

	for node in tree.postorder_node_iter():
		if node.is_leaf():
			if node.Seq==-1:
				node.popen=[]
				node.esl=[]
				node.LK=[]
				for i in range(nResidues):
					node.popen.append(1.0)
					node.esl.append(1.0)
					node.LK.append(1.0)
			else:
				node.popen=[]
				node.esl=[]
				node.LK=[]
				for i in range(nResidues):
					node.popen.append(0.0)
					node.esl.append(0.0)
					node.LK.append(0.0)
				node.popen[node.Seq]=1.0
				node.esl[node.Seq]=1.0
				node.LK[node.Seq]=1.0
			continue
		children=node.child_nodes()
		assert len(children)==2
		
		if modelType=="HKY" or modelType=="JC":
			p1=mutProbsNew(children[0].edge_length, pi[pos], kappa=1.0, qM=None, modelType=modelType)
		else:
			p1=mutProbsNew(children[0].edge_length, pi[pos], kappa=1.0, qM=qMat, modelType=modelType)
		children[0].popenatparent=[]
		for i in range(nResidues):
			children[0].popenatparent.append(children[0].popen[i]*(1.0-p1[i]))
		
		if modelType=="HKY" or modelType=="JC":
			p2=mutProbsNew(children[1].edge_length, pi[pos], kappa=1.0, qM=None, modelType=modelType)
		else:
			p2=mutProbsNew(children[1].edge_length, pi[pos], kappa=1.0, qM=qMat, modelType=modelType)
		children[1].popenatparent=[]
		for i in range(nResidues):	
			children[1].popenatparent.append(children[1].popen[i]*(1.0-p2[i]))
		
		pMatrix1=mutProbs(children[0].edge_length, pi[pos], kappa=1.0, qM=qMat, modelType=modelType)
		pMatrix2=mutProbs(children[1].edge_length, pi[pos], kappa=1.0, qM=qMat, modelType=modelType)
			
		#likelihood and score up the branch
		node.LK=[]
		children[0].eslUp=[]
		children[1].eslUp=[]
		children[0].LKup=[]
		children[1].LKup=[]
		for i in range(nResidues):
			sub1=0.0
			sub2=0.0
			children[0].eslUp.append(0.0)
			children[1].eslUp.append(0.0)
			for j in range(nResidues):
				sub1+=pMatrix1[i][j]*children[0].LK[j]
				sub2+=pMatrix2[i][j]*children[1].LK[j]
				children[0].eslUp[i]+=pMatrix1[i][j]*children[0].esl[j]
				children[1].eslUp[i]+=pMatrix2[i][j]*children[1].esl[j]
			children[0].LKup.append(sub1)
			children[1].LKup.append(sub2)
			node.LK.append(sub1*sub2)
		
		#likelihood of the subtree data and that there is at least one leaf Phylogenetically IBD to this node (given each nucleotide at current node)
		node.popen=[]
		for i in range(nResidues):
			node.popen.append(children[0].popenatparent[i]*children[1].LKup[i] + children[1].popenatparent[i]*children[0].LKup[i] - children[0].popenatparent[i]*children[1].popenatparent[i])
		
		#the effective number of sequences of the subtree at the current node, conditional on ancestral state
		node.esl=[]
		for i in range(nResidues):
			node.esl.append(children[0].eslUp[i]*children[1].LKup[i] + children[1].eslUp[i]*children[0].LKup[i] - children[0].popenatparent[i]*children[1].popenatparent[i])

	for node in tree.postorder_node_iter():
		if node.label==None:
			node.label=""
		node.label=node.label+"_"+str(node.esl)+"_"
	
	finalScore=0.0
	finalLK=0.0
	for i in range(nResidues):
		finalScore+=pi[pos][i]*tree.seed_node.esl[i]
		finalLK+=pi[pos][i]*tree.seed_node.LK[i]
	finalScore=finalScore/finalLK
	return finalScore




	



	
#given a simulation of mutation events, count how many different states are at the leaves of the tree.
def assignStatesNew(tree,conf):
	nstates=1
	for node in tree.preorder_node_iter():
		if node.edge_length==None or node.parent_node==None:
			node.state=1
			continue
		if (conf>>(node.id-1)) % 2 == 1:
			nstates+=1
			node.state=nstates
		else:
			node.state=node.parent_node.state
	leafstates=[node.state for node in tree.leaf_nodes()]
	return len(set(leafstates))

#Return probability matrices
def calcProbsNew(tree,F):
	probs=[]
	#print("lengths: ")
	for node in tree.preorder_node_iter():
		if node.edge_length==None or node.parent_node==None:
			node.state=1
			continue
		probs.append(mutProbs(node.edge_length,F))
		#print(node.edge_length)
	return probs

	


#return probability of a configuration of mutational events and ancestral-observed states, this time has to be consistent with data
def confProbData(tree,conf,mutprobs,states,F, nResidues=4):
	nodes=[node for node in tree.preorder_node_iter()]
	conflist=[(conf>>(node.id-1))%2 for node in nodes[1:]]
	nM=nMuts(tree,conf)
	stateslist=[(states>>(i*2))%nResidues for i in range(nM+1)]
	prob=pi[0][stateslist[0]]
	
	mut=0
	n=0
	for node in tree.preorder_node_iter():
		if node.edge_length==None or node.parent_node==None:
			node.state2=stateslist[0]
		else:
			if conflist[n-1]==0:
				node.state2=node.parent_node.state2
				if node.is_leaf():
					if node.Seq!=-1:
						if node.Seq!=node.state2:
							return 0.0
				prob*=(1.0-mutProbsNew(node.edge_length,F)[node.state2])
			else:
				mut+=1
				node.state2=stateslist[mut]
				if node.is_leaf():
					if node.Seq!=-1:
						if node.Seq!=node.state2:
							return 0.0
				stateC=node.state2
				stateP=node.parent_node.state2
				if stateC!=stateP:
					prob*=mutprobs[n-1][stateP][stateC]
				else:
					prob*=(mutprobs[n-1][stateP][stateC]-(1.0-mutProbsNew(node.edge_length,F)[node.state2]))
		n+=1
	#problist=map(lambda x,y: y if x==1 else 1.0-y,conflist,mutprobs)
	#print "problist"+str(problist)

	#prob=reduce(mul,problist)
	return prob
	



#Number of mutated branches in mutation vector
def nMuts(tree,conf):
	n=0
	for node in tree.preorder_node_iter():
		if node.edge_length==None or node.parent_node==None:
			#node.state=1
			continue
		if (conf>>(node.id-1)) % 2 == 1:
			n+=1
	return n




#considers all possible assignments of mutation events (presence/absence) on the tree branches, and assignments of node states (this time consistent with alignment column), each with its own probability,
#for each assignment it counts the effective sequence score, and then calculates the mean score. This version also calculates
#the score for each tip of the tree
#MIGHT NOT WORK ANYMORE AFTER AA CHANGES
def exhaustiveEffSeqLenLeavesData(tree, pos=0, modelType="JC"):
	names=[]
	for node in tree.leaf_nodes():
		names.append(node.taxon.label.replace(" ","_"))
	elements=[]
	if modelType=="LG":
		nResidues=20
		qMat=LGqM(pi[pos])
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
		qMat=nucqM(pi[pos],modelType=modelType,k=1.0)
	for i in range(nResidues):
		elements.append(i)
		
	edgecnt=0
	for node in tree.preorder_node_iter():
		node.id=edgecnt
		edgecnt+=1
	eslhist=[0.0 for node in tree.leaf_nodes()]
	leafScores=[0.0]*len(tree.leaf_nodes())
	mutprobs=calcProbsNew(tree,pi[pos])
	totProb=0.0
	mProbs=[]
	nMs=[]
	for i in range(1<<(edgecnt-1)):
		esl=assignStatesNew(tree,i)
		mutationalProb=0.0
		nM=nMuts(tree,i)
		nMs.append(nM)
		for j in range(1<<((nM+1)*2)):  
			prob=confProbData(tree,i,mutprobs,j,pi[pos],nResidues=nResidues)
			mutationalProb+=prob
		totProb+=mutationalProb
		mProbs.append(mutationalProb)
	
	for i in range(1<<(edgecnt-1)):
		mProbs[i]=mProbs[i]/totProb
		esl=assignStatesNew(tree,i)
		stateAbundances=[0]*(2*edgecnt)
		for node in tree.leaf_nodes():
			stateAbundances[node.state-1]+=1
		leafCount=0
		for node in tree.leaf_nodes():
			if(stateAbundances[node.state-1]<1):
				print("error: the abundance of state "+str(node.state)+" should be positive")
				exit()
			leafScores[leafCount]+=(1.0/stateAbundances[node.state-1])*mProbs[i]
			leafCount+=1
		eslhist[esl-1]+=mProbs[i]
	meanesl=sum(map(lambda x,y:(x+1)*y,range(len(eslhist)),eslhist))
	return leafScores,names,eslhist,meanesl












def extendBranchUpNew(pDown,p,P, nResidues):
	vec=[]
	totP=[0.0]*nResidues
	for i in range(nResidues):
		lv=len(pDown[i])
		vec.append([0.0]*lv)
		for j in range(lv):
			totP[i]+=pDown[i][j]
	for i in range(nResidues):
		vec[i][0]=(P[i][i]-(1.0-p[i]))*totP[i]+pDown[i][0]*(1.0-p[i])
		for l in range(nResidues):
			if (l!=i):
				vec[i][0]+=P[i][l]*totP[l]
		lv=len(pDown[i])
		for j in range(lv-1):
			vec[i][j+1]=pDown[i][j+1]*(1.0-p[i])
	return vec
	
def extendBranchDownNew(pUp,p,P, nResidues):
	vec=[]
	totP=[0.0]*nResidues
	for i in range(nResidues):
		lv=len(pUp[i])
		vec.append([0.0]*lv)
		for j in range(lv):
			totP[i]+=pUp[i][j]
	for i in range(nResidues):
		vec[i][0]=(P[i][i]-(1.0-p[i]))*totP[i]+pUp[i][0]*(1.0-p[i])
		lv=len(pUp[i])
		for l in range(nResidues):
			if (l!=i):
				vec[i][0]+=P[l][i]*totP[l]
		for j in range(lv-1):
			vec[i][j+1]=pUp[i][j+1]*(1.0-p[i])
	return vec
	
	
	
#combine the conditional probabilities of two subphylogenies for one node
def combineSubPhyloNew(pDown1,pDown2, nResidues):
	pCombo=[]
	for j in range(nResidues):
		l1=len(pDown1[j])
		l2=len(pDown2[j])
		if l1>=l2:
			range1=l2-1
			range2=l1-1
			p1=pDown2
			p2=pDown1
		else:
			range1=l1-1
			range2=l2-1
			p1=pDown1
			p2=pDown2
		l=l1+l2-1
		pCombo.append([0.0]*l)
		for i in range(l):
			start=0
			if i>range2:
				start=i-range2
			end=range1+1
			if i+1<end:
				end=i+1
			if (range1+1-start)<end:
				end=range1+1-start
			#print(str(range1+1)+" "+str(i+1)+" "+str(range1+1-start))
			for k in range(end):
				pCombo[j][i]+=p1[j][k+start]*p2[j][i-(k+start)]
		#print("new cycle: "+str(i)+" start: "+str(start)+" end "+str(end)+" final value: "+str(pCombo[i]))
	return pCombo

		

#update conditional probabilities of a child node, recursively call it on its own children
def updateConditionalsNew(node,l,pParent, F, nResidues, kappa=1.0):
	p=node.p1
	node.pUp=extendBranchDownNew(pParent,p,node.P, nResidues)
	children=node.child_nodes()
	if node.is_leaf():
		node.final=combineSubPhyloNew(node.pUp,node.down, nResidues)
	elif len(children)==1:
		updateConditionalsNew(children[0],children[0].edge_length,node.pUp, F, nResidues, kappa=kappa)
	elif len(children)==2:
		node.upRight=combineSubPhyloNew(node.pUp,node.down2, nResidues)
		node.upLeft=combineSubPhyloNew(node.pUp,node.down1, nResidues)
		updateConditionalsNew(children[0],children[0].edge_length,node.upRight, F, nResidues, kappa=kappa)
		updateConditionalsNew(children[1],children[1].edge_length,node.upLeft, F, nResidues, kappa=kappa)
		

#calculate final leaf score from probability distribution
def scoreNew(p, nResidues):
	tot=0.0
	for j in range(nResidues):
		l=len(p[j])
		s=0.0
		for i in range(l-1):
			s+=p[j][i+1]/(i+1)
		tot+=s
	return tot

	

#weigh distributions using root frequencies
def weightByFrequencies(probs):
	newP=[]
	for nuc in range(len(probs)):
		newP.append([])
		for i in range(len(probs[nuc])):
			newP[nuc].append(probs[nuc][i]*pi[0][nuc])
	return newP
	
	


#Fast algorithm to calculate scores at the leaves
def effSeqLenLeavesData(tree, noData=False, F=[0.25,0.25,0.25,0.25], fast=False, pos=0, k=1.0, modelType="JC"):
	if modelType=="LG":
		nResidues=20
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
	if (not fast) or pos==0:
		if modelType=="LG":
			qMat=LGqM(F)
		elif modelType=="GTR":
			qMat=nucqM(F,modelType=modelType,k=k)
	# elements=[]
# 	if modelType=="LG":
# 		nResidues=20
# 		qMat=LGqM(F)
# 	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
# 		nResidues=4
# 		qMat=nucqM(pi[pos],modelType=modelType,k=k)
# 	for i in range(nResidues):
# 		elements.append(i)
	for node in tree.postorder_node_iter():
		if node.is_leaf():
			if node.Seq==-1 or noData:
				node.down=[[0.0,1.0]]*nResidues
				#print node.down
				#node.down=[[0.0,1.0],[0.0,1.0],[0.0,1.0],[0.0,1.0]]
			else:
				node.down=[[0.0]]*nResidues
				if fast:
					node.down[node.Seq[pos]].append(1.0)
				else:
					node.down[node.Seq].append(1.0)
			#continue
		children=node.child_nodes()
		#print len(children)
		if len(children)==2:
			if (not fast) or pos==0:
				if modelType=="HKY" or modelType=="JC":
					children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[1].P=mutProbs(children[1].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[1].p1=mutProbsNew(children[1].edge_length, F, kappa=k, qM=None, modelType=modelType)
				else:
					children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[1].P=mutProbs(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[1].p1=mutProbsNew(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
			node.down1=extendBranchUpNew(children[0].down,children[0].p1,children[0].P, nResidues)
			node.down2=extendBranchUpNew(children[1].down,children[1].p1,children[1].P, nResidues)
			node.down=combineSubPhyloNew(node.down1,node.down2, nResidues)
		elif len(children)==1:
			if (not fast) or pos==0:
				children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
			node.down1=extendBranchUpNew(children[0].down,children[0].p1,children[1].P, nResidues)
			node.down=node.down1
			
	#Calculated all probabilities conditional on subphylogenies facing down.
	#Now calculate all other conditional probabilities
	root=tree.seed_node
	root.final=root.down
	#print("Before")
	#print(root.final)
	root.final=weightByFrequencies(root.final)
	LK=0.0
	for nuc in range(len(root.final)):
		for i in range(len(root.final[nuc])):
			LK+=root.final[nuc][i]
	#print("After")
	#print(root.final)
	children=root.child_nodes()
	if len(children)==0:
		return [scoreNew(root.final, nResidues)]
	root.upLeft=weightByFrequencies(root.down1)
	if len(children)==2:
		root.upRight=weightByFrequencies(root.down2)
		updateConditionalsNew(children[0],children[0].edge_length,root.upRight, F, nResidues, kappa=k)
		updateConditionalsNew(children[1],children[1].edge_length,root.upLeft, F, nResidues, kappa=k)
	else:
		updateConditionalsNew(children[0],children[0].edge_length,[[1.0]]*nResidues, F, nResidues, kappa=k)
		
	scores=[]
	names=[]
	for node in tree.postorder_node_iter():
		if node.label==None:
			node.label=""
		if node.is_leaf():
			#print(node.final)
			s=scoreNew(node.final, nResidues)
			s=s/LK
			names.append(node.taxon.label.replace(" ","_"))
			#node.label=node.label+"_"+str(s)+"_"
			scores.append(s)
	
	return names,scores
	

	





























	

	

#update conditional probabilities of a child node, recursively call it on its own children
def updateConditionalsNewFast(node,l,pParent,pParentL, F, nResidues, kappa=1.0):
	#print("iteration, blen "+str(l))
	nRes=nResidues
	p=[]#mutProbsNew(l, F, kappa=kappa)
	for resi in range(nRes):
		p.append(1.0-node.p1[resi])
	pP=node.P
	node.pUp=[]
	node.pUpL=[]
	for resi in range(nRes):
		node.pUp.append(pParent[resi]*p[resi])
		node.pUpL.append(0.0)
		for resi2 in range(nRes):
			node.pUpL[resi]+=pParentL[resi2]*pP[resi2][resi]
	children=node.child_nodes()
	if node.is_leaf():
		node.final=[]
		for resi in range(nRes):
			node.final.append(node.pUp[resi]*node.downL[resi]+node.pUpL[resi]*node.down[resi])
	elif len(children)==1:
		updateConditionalsNewFast(children[0],children[0].edge_length,node.pUp,node.pUpL, F, nResidues, kappa=kappa)
	elif len(children)==2:
		node.upRight=[]
		node.upRightL=[]
		for resi in range(nRes):
			node.upRight.append(node.down2[resi]*node.pUpL[resi] + node.down2L[resi]*node.pUp[resi])
			node.upRightL.append(node.pUpL[resi]*node.down2L[resi])
		node.upLeft=[]
		node.upLeftL=[]
		for resi in range(nRes):
			node.upLeft.append(node.down1[resi]*node.pUpL[resi] + node.down1L[resi]*node.pUp[resi])
			node.upLeftL.append(node.pUpL[resi]*node.down1L[resi])
		updateConditionalsNewFast(children[0],children[0].edge_length,node.upRight,node.upRightL, F, nResidues, kappa=kappa)
		updateConditionalsNewFast(children[1],children[1].edge_length,node.upLeft,node.upLeftL, F, nResidues, kappa=kappa)


#weigh distributions using root frequencies
def weightByFrequenciesFast(probs):
	newP=[]
	for nuc in range(len(probs)):
		newP.append(probs[nuc]*pi[0][nuc])
	return newP


#Fast algorithm to calculate scores at the leaves - now even faster by calculating the 1/E[NPIBD] instead of the E[1/NPIBD]
def effSeqLenLeavesDataFast(tree, noData=False, F=[0.25,0.25,0.25,0.25], fast=False, pos=0, k=1.0, modelType="JC"):
	if modelType=="LG":
		nResidues=20
	elif modelType=="HKY" or modelType=="GTR" or modelType=="JC":
		nResidues=4
	if (not fast) or pos==0:
		if modelType=="LG":
			qMat=LGqM(F)
		elif modelType=="GTR":
			qMat=nucqM(F,modelType=modelType,k=k)
	nRes=nResidues
	for node in tree.postorder_node_iter():
		if node.is_leaf():
			if node.Seq==-1 or noData:
				node.down=[1.0]*nResidues
				node.downL=[1.0]*nResidues
			else:
				node.down=[0.0]*nResidues
				node.downL=[0.0]*nResidues
				if fast:
					node.down[node.Seq[pos]]=1.0
					node.downL[node.Seq[pos]]=1.0
				else:
					node.down[node.Seq]=1.0
					node.downL[node.Seq]=1.0
			#continue
		children=node.child_nodes()
		if len(children)==2:
			if (not fast) or pos==0:
				if modelType=="HKY" or modelType=="JC":
					children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[1].P=mutProbs(children[1].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=None, modelType=modelType)
					children[1].p1=mutProbsNew(children[1].edge_length, F, kappa=k, qM=None, modelType=modelType)
				else:
					children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[1].P=mutProbs(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
					children[1].p1=mutProbsNew(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				#children[0].P=mutProbs(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				#children[1].P=mutProbs(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				#children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
				#children[1].p1=mutProbsNew(children[1].edge_length, F, kappa=k, qM=qMat, modelType=modelType)
			pp1=[]#mutProbsNew(l, F, kappa=kappa)
			for resi in range(nRes):
				pp1.append(1.0-children[0].p1[resi])
			pp2=[]#mutProbsNew(l, F, kappa=kappa)
			for resi in range(nRes):
				pp2.append(1.0-children[1].p1[resi])
			#print("before extension")
			#print(children[0].down)
			node.down1=[]
			node.down1L=[]
			for resi in range(nRes):
				node.down1.append(children[0].down[resi]*pp1[resi])
				node.down1L.append(0.0)
				for resi2 in range(nRes):
					node.down1L[resi]+=children[0].downL[resi2]*children[0].P[resi][resi2]

			node.down2=[]
			node.down2L=[]
			for resi in range(nRes):
				node.down2.append(children[1].down[resi]*pp2[resi])
				node.down2L.append(0.0)
				for resi2 in range(nRes):
					node.down2L[resi]+=children[1].downL[resi2]*children[1].P[resi][resi2]

			node.down=[]
			node.downL=[]
			for resi in range(nRes):
				node.down.append(node.down2[resi]*node.down1L[resi] + node.down1[resi]*node.down2L[resi])
				node.downL.append(node.down1L[resi]*node.down2L[resi])

		elif len(children)==1:
			if (not fast) or pos==0:
				children[0].p1=mutProbsNew(children[0].edge_length, F, kappa=k)
				children[0].P=mutProbs(children[0].edge_length, F, kappa=k)
			node.down1=[]
			node.down1L=[]
			for resi in range(nRes):
				node.down1.append(children[0].down[resi]*(1.0-children[0].p1[resi]))
				node.down1L.append(0.0)
				for resi2 in range(nRes):
					node.down1L[resi]+=children[0].downL[resi2]*children[0].P[resi][resi2]
					
			#node.down1=extendBranchUpNew(children[0].down,children[0].p1,children[1].P)
			node.down=node.down1
			node.downL=node.down1L
			
	#Calculated all probabilities conditional on subphylogenies facing down.
	#Now calculate all other conditional probabilities
	root=tree.seed_node

	final=weightByFrequenciesFast(root.downL)
	LK=0.0
	for nuc in range(nRes):
		LK+=final[nuc]

	children=root.child_nodes()
	if len(children)==0:
		return LK/sum(weightByFrequenciesFast(root.down))
	root.upLeft=weightByFrequenciesFast(root.down1)
	root.upLeftL=weightByFrequenciesFast(root.down1L)
	if len(children)==2:
		root.upRight=weightByFrequenciesFast(root.down2)
		root.upRightL=weightByFrequenciesFast(root.down2L)
		updateConditionalsNewFast(children[0],children[0].edge_length,root.upRight,root.upRightL, F, nResidues, kappa=k)
		updateConditionalsNewFast(children[1],children[1].edge_length,root.upLeft,root.upLeftL, F, nResidues, kappa=k)
	else:
		updateConditionalsNewFast(children[0],children[0].edge_length,root.upLeft,root.upLeftL, F, nResidues, kappa=k)
		
	scores=[]
	names=[]
	for node in tree.postorder_node_iter():
		if node.label==None:
			node.label=""
		if node.is_leaf():
			s=sum(node.final)
			s=LK/s
			names.append(node.taxon.label.replace(" ","_"))
			#names.append(node.label)
			scores.append(s)
	
	return names,scores






























#weighting scheme from Gerstin, Sonnhammer and Chothia, 1994.
def GSCweights(tree):
	for node in tree.postorder_node_iter():
		if node.is_leaf():
			node.final=node.edge_length
			node.nList=[node]
			if node.final<0.000001:
				node.final=0.000001
			if not (type(node.final) is float):
				exit()

		children=node.child_nodes()
		if len(children)==2 and (type(node.edge_length) is float):
			node.nList=children[0].nList+children[1].nList
			#print node.edge_length
			#print node.nList
			sum1=0.0
			for node2 in node.nList:
				sum1+=node2.final
			for node2 in node.nList:
				node2.final=node2.final + node.edge_length*(node2.final/sum1)
	scores=[]
	names=[]
	
	for node in tree.postorder_node_iter():
		if node.label==None:
			node.label=""
		if node.is_leaf():
			names.append(node.taxon.label.replace(" ","_"))
			scores.append(node.final)
	
	return names,scores




















#weighting scheme from Henikoff and Henikoff, 1994.
def HH94weights(alignment):
	N=len(alignment)
	L=len(alignment[0])
	weights=[]
	for j in range(N):
		weights.append(0.0)
	for i in range(L):
		residues={}
		for j in range(N):
			if alignment[j][i] in residues.keys():
				residues[alignment[j][i]]=residues[alignment[j][i]]+1
			else:
				residues[alignment[j][i]]=1
		for j in range(N):
			weights[j]+=1.0/(len(residues.keys())*residues[alignment[j][i]])
	return weights






































#Infer equilibrium/root frequencies using BEAST2
def runBEAST2(tree, modelType="JC"):
	DNA=["A","C","G","T"]
	fileBEAST=open(args.output+".xml","w")
	fileBEAST.write("<beast version=\'2.0\' namespace=\'beast.evolution.alignment:beast.core:beast.core.parameter:beast.evolution.tree:beast.evolution.tree.coalescent:beast.core.util:beast.evolution.operators:beast.evolution.sitemodel:beast.evolution.substitutionmodel:beast.evolution.likelihood:beast.evolution.tree:beast.math.distributions\'>\n\n")
	fileBEAST.write("<data id=\"alignment\" dataType=\"nucleotide\">\n")
	#if modelType=="LG":
	#	alphabet=AAs
	#else:
	#	alphabet=DNA
	for node in tree.leaf_nodes():
		fileBEAST.write("<sequence taxon=\'"+node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"\' value=\'"+alphabet[node.Seq]+"\'/>\n")
	fileBEAST.write("</data>\n\n\n")
	
	fileBEAST.write("<siteModel spec=\"SiteModel\" id=\"siteModel\">\n <mutationRate spec=\'RealParameter\' id=\"mutationRate\" value=\"1.0\"/>\n  <substModel spec=\"HKY\">\n    <kappa spec=\'RealParameter\' id=\"hky.kappa\" value=\"3.0\"/>\n     <frequencies estimate=\"false\" spec=\'Frequencies\'>\n <frequencies spec=\'RealParameter\' id=\"hky.freq\" value=\"0.25 0.25 0.25 0.25\"  lower=\"0.0\" upper=\"1.0\"/>\n    </frequencies>\n     </substModel>\n   </siteModel>\n\n")

	fileBEAST.write("<input spec=\'TreeLikelihood\' id=\"treeLikelihood\">\n     <data idref=\"alignment\"/>\n     <tree idref=\"tree\"/>\n     <siteModel idref=\'siteModel\'/>\n   </input>\n\n\n")

	fileBEAST.write("<run spec=\"MCMC\" id=\"mcmc\" chainLength=\"100000\" storeEvery=\"10000\">\n")
	
	fileBEAST.write("<init spec=\'beast.util.TreeParser\' id=\'NewickTree\' taxa=\"@alignment\"  initial=\"@tree\" IsLabelledNewick=\"true\"   newick=\""+tree.as_string(schema="newick",).replace("\n","").replace("'","").replace("\'","").replace(" ","_")+"\"/>\n\n\n")
    
	fileBEAST.write("<state>\n   <tree id=\"tree\" name=\"stateNode\"/>\n <stateNode idref=\"mutationRate\"/>\n <stateNode idref=\"hky.kappa\"/>\n <stateNode idref=\"hky.freq\"/>\n </state>\n\n")
	fileBEAST.write("<distribution spec=\'CompoundDistribution\' id=\'posterior\'>\n <distribution idref=\"treeLikelihood\"/>\n </distribution>\n\n")
	fileBEAST.write("<operator spec=\"DeltaExchangeOperator\" id=\"freqExchanger\"  parameter=\"@hky.freq\"    delta=\"0.01\" weight=\"0.1\"/>\n\n")
	fileBEAST.write("<logger logEvery=\"50\" fileName=\""+args.output+"_BeastOutput.txt\">\n   <log idref=\"hky.freq\"/>\n </logger>\n\n   </run>\n </beast>\n")

	fileBEAST.close()
	
	os.system("java -Djava.awt.headless=true -cp /Users/demaio/Desktop/BEAST2_workspace/beast2/release/package/beast2.5.0/lib/beast.jar  beast.app.beastapp.BeastMain -seed 1 -overwrite "+args.output+".xml")
	
	samples=[[],[],[],[]]
	fileTrace=open(args.output+"_BeastOutput.txt")
	line=fileTrace.readline()
	line=fileTrace.readline()
	while line!="" and line!="\n":
		linelist=line.split()
		for i in range(4):
			samples[i].append(float(linelist[i+1]))
		line=fileTrace.readline()
	fileTrace.close()
	means=[]
	for i in range(4):
		means.append(sum(samples[i])/len(samples[i]))
	return means






#Infer equilibrium/root frequencies and tree and subs model using BEAST2
def runBEAST2gene(tree):
	DNA=["A","C","G","T"]
	fileBEAST=open(args.output+".xml","w")
	fileBEAST.write("<beast version=\'2.0\' namespace=\'beast.evolution.alignment:beast.core:beast.core.parameter:beast.evolution.tree:beast.evolution.tree.coalescent:beast.core.util:beast.evolution.operators:beast.evolution.sitemodel:beast.evolution.substitutionmodel:beast.evolution.likelihood:beast.evolution.tree:beast.math.distributions\'>\n\n")
	leaves=tree.leaf_nodes()
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write("<data id=\"alignment"+str(p)+"\" dataType=\"nucleotide\">\n")
		for node in leaves:
			fileBEAST.write("<sequence taxon=\'"+node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"\' value=\'"+DNA[node.Seq[p]]+"\'/>\n")
		fileBEAST.write("</data>\n\n\n")
	
	fileBEAST.write("<siteModel spec=\"SiteModel\" id=\"siteModel0\">\n <mutationRate spec=\'RealParameter\' id=\"mutationRate\" value=\"1.0\"/>\n  <substModel spec=\"HKY\">\n    <kappa spec=\'RealParameter\' id=\"hky.kappa\" value=\"1.0\"/>\n     <frequencies estimate=\"true\" spec=\'Frequencies\'>\n <frequencies spec=\'RealParameter\' id=\"hky.freq0\" value=\"0.25 0.25 0.25 0.25\"  lower=\"0.0\" upper=\"1.0\"/>\n    </frequencies>\n     </substModel>\n   </siteModel>\n\n")
	for p in range(len(leaves[0].Seq)-1):
		fileBEAST.write("<siteModel spec=\"SiteModel\" id=\"siteModel"+str(p+1)+"\" mutationRate=\"@mutationRate\">\n  <substModel spec=\"HKY\" kappa=\"@hky.kappa\">\n     <frequencies estimate=\"false\" spec=\'Frequencies\'>\n <frequencies spec=\'RealParameter\' id=\"hky.freq"+str(p+1)+"\" value=\"0.25 0.25 0.25 0.25\"  lower=\"0.0\" upper=\"1.0\"/>\n    </frequencies>\n     </substModel>\n   </siteModel>\n\n")
	
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write("<input spec=\'TreeLikelihood\' id=\"treeLikelihood"+str(p)+"\">\n     <data idref=\"alignment"+str(p)+"\"/>\n     <tree idref=\"tree\"/>\n     <siteModel idref=\'siteModel"+str(p)+"\'/>\n   </input>\n\n\n")


	fileBEAST.write("<input spec=\'beast.evolution.speciation.YuleModel\' id=\"yule\">\n        <parameter name=\'birthDiffRate\' id=\"birthRate\" value=\"1\"/>\n            <tree spec=\'beast.evolution.tree.RandomTree\' id=\'tree\' estimate=\'true\'>\n                <taxa spec=\'Alignment\' idref=\'alignment1\'/>                <populationModel id=\'ConstantPopulation0\' spec=\'beast.evolution.tree.coalescent.ConstantPopulation\'>\n            		<popSize id=\'randomPopSize\' spec=\'parameter.RealParameter\' value=\'1\'/>\n	            </populationModel>\n            </tree>\n     </input>\n")
	
	
	fileBEAST.write("<run spec=\"MCMC\" id=\"mcmc\" chainLength=\"500000\" storeEvery=\"10000\">\n")
    
    #   <tree id=\"tree\" name=\"stateNode\"/>\n
	fileBEAST.write("<state>\n <stateNode idref=\"tree\"/>\n <stateNode idref=\"mutationRate\"/>\n <stateNode idref=\"birthRate\"/>\n <stateNode idref=\"hky.kappa\"/>\n")
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write(" <stateNode idref=\"hky.freq"+str(p)+"\"/>\n")
	fileBEAST.write(" </state>\n\n")
	
	fileBEAST.write("<distribution spec=\'CompoundDistribution\' id=\'posterior\'>\n")
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write(" <distribution idref=\"treeLikelihood"+str(p)+"\"/>\n")
		
	fileBEAST.write("<distribution spec=\'beast.math.distributions.Prior\' x=\"@hky.kappa\">\n       <distr spec=\'LogNormalDistributionModel\' M=\"0.0\" S=\"4.0\"/>\n     </distribution>\n")
	
	
	fileBEAST.write("<distribution spec=\'beast.math.distributions.Prior\' x=\"@birthRate\">\n       <distr spec=\'LogNormalDistributionModel\' M=\"0.0\" S=\"4.0\"/>\n     </distribution>\n")
     
     
	fileBEAST.write("<distribution idref=\"yule\"/>\n")
     
	fileBEAST.write(" </distribution>\n\n")
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write("<operator spec=\"DeltaExchangeOperator\" id=\"freqExchanger"+str(p)+"\"  parameter=\"@hky.freq"+str(p)+"\"    delta=\"0.01\" weight=\"0.1\"/>\n\n")
	
	fileBEAST.write("<operator spec=\"ScaleOperator\" id=\"kappaScaler\"   parameter=\"@hky.kappa\"  scaleFactor=\"0.8\" weight=\"1.0\"/>\n")
	
	fileBEAST.write("<operator spec=\"ScaleOperator\" id=\"birthScaler\"   parameter=\"@birthRate\"  scaleFactor=\"0.8\" weight=\"1.0\"/>\n")
	
	fileBEAST.write("<operator id=\'treeScaler\' spec=\'ScaleOperator\' scaleFactor=\"0.75\" weight=\"5\" tree=\"@tree\"/>\n        <operator spec=\'beast.evolution.operators.Uniform\' weight=\"30\" tree=\"@tree\"/>\n        <operator spec=\'SubtreeSlide\' weight=\"15\" gaussian=\"true\" size=\"0.0077\" tree=\"@tree\"/>\n        <operator id=\'narrow\' spec=\'Exchange\' isNarrow=\'true\' weight=\"15\" tree=\"@tree\"/>\n        <operator id=\'wide\' spec=\'Exchange\' isNarrow=\'false\' weight=\"5\" tree=\"@tree\"/>\n        <operator spec=\'WilsonBalding\' weight=\"5\" tree=\"@tree\"/>\n")
	
	fileBEAST.write("<logger logEvery=\"2500\" fileName=\""+args.output+"_BeastOutput.txt\">\n   <log idref=\"hky.kappa\"/>\n   <log idref=\"birthRate\"/>\n\n")
	for p in range(len(leaves[0].Seq)):
		fileBEAST.write("   <log idref=\"hky.freq"+str(p)+"\"/>\n")
	fileBEAST.write(" </logger>\n\n") 
	fileBEAST.write("   </run>\n") 
	fileBEAST.write(" </beast>\n") 
        
	fileBEAST.close()
	
	os.system("java -Djava.awt.headless=true -cp /Users/demaio/Desktop/BEAST2_workspace/beast2/release/package/beast2.5.0/lib/beast.jar  beast.app.beastapp.BeastMain -seed 1 -overwrite "+args.output+".xml")
	
	
	samples=[]
	for p in range(len(leaves[0].Seq)):
		samples.append([[],[],[],[]])
	
	fileTrace=open(args.output+"_BeastOutput.txt")
	line=fileTrace.readline()
	for i in range(40):
		line=fileTrace.readline()
	while line!="" and line!="\n":
		linelist=line.split()
		for p in range(len(leaves[0].Seq)):
			for i in range(4):
				samples[p][i].append(float(linelist[p*4+3+i]))
		line=fileTrace.readline()
	fileTrace.close()
	means=[]
	for p in range(len(leaves[0].Seq)):
		means.append([])
		for i in range(4):
			means[p].append(sum(samples[p][i])/len(samples[p][i]))
	return means














#Infer equilibrium/root frequencies using PAML v4.8
def runPAML(tree):
	DNA=["A","C","G","T"]
	leaves=tree.leaf_nodes()
	filePAML=open(args.output+"_PAML.ctl","w")
	filePAML.write("      seqfile = "+args.output+"_PAML_seqs.nuc\n")
	filePAML.write("      treefile = "+args.output+"_PAML_tree.trees\n")
	filePAML.write("      outfile = "+args.output+"_PAML_output\n")
	filePAML.write("    noisy = 0\n    verbose = 0\n      cleandata = 0\n      runmode = 0\n       method = 0\n        clock = 1\n        Mgene = 0\n        nhomo = 1\n        getSE = 0\n RateAncestor = 0\n        model = 4\n    fix_kappa = 1\n        kappa = 3\n    fix_alpha = 1\n        alpha = 0\n       Malpha = 0\n        ncatG = 1\n      fix_rho = 1\n          rho = 0\n        nparK = 0\n        ndata = 1\n   Small_Diff = 7e-6\n  fix_blength = 2\n")
	filePAML.close()
	
	filePAML=open(args.output+"_PAML_seqs.nuc","w")
	filePAML.write("  "+str(len(leaves))+"   1\n\n")
	#added=0
	for node in tree.leaf_nodes():
		#if added==0:
			filePAML.write(node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"   "+DNA[node.Seq]+"\n\n")
			#ACGT
		#	added=1
		#else:
		#	filePAML.write(node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"   "+DNA[node.Seq]+"----\n\n")
			
	filePAML.close()
	
	filePAML=open(args.output+"_PAML_tree.trees","w")
	filePAML.write("  "+str(len(leaves))+"   1\n\n")
	filePAML.write(tree.as_string(schema="newick",).replace("\n","").replace("'","").replace("\'","").replace(" ","_")+"\n\n")
	filePAML.close()
	
	os.system("/Applications/paml4.8/bin/baseml "+args.output+"_PAML.ctl")
	
	freqs=[]
	fileTrace=open(args.output+"_PAML_output")
	line=fileTrace.readline()
	while line!="Base frequencies:\n":
		line=fileTrace.readline()
	line=fileTrace.readline()
	linelist=line.split()
	freqs.append(linelist[2])
	freqs.append(linelist[1])
	freqs.append(linelist[3])
	freqs.append(linelist[0])
	fileTrace.close()
	return freqs
	
	













#Infer equilibrium/root frequencies using PhyML v3.1
def runPhyML(tree, pos=0, kappaML=3.0, append=False):
	DNA=["A","C","G","T"]
	leaves=tree.leaf_nodes()
	filePAML=open(args.output+"_PhyML_data.txt","w")
	filePAML.write("	"+str(len(leaves))+"	1\n")
	for node in tree.leaf_nodes():
		if append:
			filePAML.write(node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"   "+DNA[node.Seq[pos]]+"\n")
		else:
			filePAML.write(node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"   "+DNA[node.Seq]+"\n")
	filePAML.close()
	filePAML=open(args.output+"_PhyML_tree.txt","w")
	filePAML.write(tree.as_string(schema="newick",).replace("\n","").replace("'","").replace("\'","").replace(" ","_")+"\n\n")
	filePAML.close()
	os.system("/Applications/PhyML-3.1/PhyML-3.1_macOS-MountainLion -i "+args.output+"_PhyML_data.txt -u "+args.output+"_PhyML_tree.txt -b 0 -d nt -f m -t "+str(kappaML)+" -v 0.0 -o r -c 1 > "+args.output+"_PhyML_screenText.txt")
	freqs=[]
	fileTrace=open(args.output+"_PhyML_data.txt_phyml_stats.txt")
	line=fileTrace.readline()
	while line!=". Nucleotides frequencies:\n":
		line=fileTrace.readline()
	for i in range(4):
		line=fileTrace.readline()
		linelist=line.split()
		freqs.append(linelist[2])
	fileTrace.close()
	return freqs
	
	
	
	
	


#Remove trifurcations in a tree (extend to multifurcations)
def splitTrifurcation(line):
	openCount=0
	commaCount=0
	ind=len(line)-1
	while ind>=0:
		if line[ind]==")":
			openCount+=1
		elif line[ind]=="(":
			openCount-=1
		if openCount==1 and line[ind]==",":
			commaCount+=1
		if commaCount==2:
			break
		ind-=1
	line1=line[:ind+1]
	line2=line[ind+1:]
	lineNew=line1+"("+line2.replace(");","):0.000001);")
	return lineNew


#resolve_polytomies

#Remove trifurcations in a tree (extend to multifurcations)
#Now replaced with the specific dendropy function (hoping it's faster)
def splitMultifurcation(line):
	#openCount=0
	#commaCount=0
	ind=0
	numCommas={}
	startInd={}
	depth=0
	#lineNew=line
	while ind<len(line):
		if line[ind]==")":
			numCommas[depth]=0
			depth-=1
		elif line[ind]=="(":
			depth+=1
			numCommas[depth]=0
			startInd[depth]=ind
		elif  line[ind]==",":
			numCommas[depth]+=1
			if numCommas[depth]==2:
				line1=line[:startInd[depth]+1]+"("+line[startInd[depth]+1:ind]+"):0.000001"+line[ind:]
				print(line1)
				return splitMultifurcation(line1)
				#line1=line[:ind+1]#indstart
				#line2=line[ind+1:]
				#lineNew=line1+"("+line2.replace(");","):0.000001);")
		ind+=1
	return line
	
#tree="\"((Human:0.00655, Chimp:0.00684, Gorilla:0.008964, Orangutan:0.01894):0.003471, Gibbon:0.02227, Gibbon2:0.02227);\""
#print tree
#print splitMultifurcation(tree)


#write a DNA alignment contained in a tree (leaf sequences) into a phylip format file
def writePhylipAl(tree,outputF, modelType="JC"):
	if modelType=="LG":
		DNA=['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']	
	else:
		DNA=["A","C","G","T"]
	
	leaves=tree.leaf_nodes()
	filePAML=open(outputF,"w")
	filePAML.write("	"+str(len(leaves))+"	"+str(len(leaves[0].Seq))+"\n")
	seqs={}
	for node in tree.leaf_nodes():
		newName=node.taxon.label.replace("'","").replace("\'","").replace(" ","_")
		filePAML.write(node.taxon.label.replace("'","").replace("\'","").replace(" ","_")+"   ")
		seqs[newName]=[]
		for p in node.Seq:
			seqs[newName].append(p)
			filePAML.write(DNA[p])
		filePAML.write("\n")
	filePAML.close()
	return seqs
	
	
	

	
#Infer equilibrium/root frequencies and tree and subs model using PhyML v3.1
def runPhyMLtree(tree):
	writePhylipAl(tree,args.output+"_PhyML_data.txt")
	
	os.system("/Applications/PhyML-3.1/PhyML-3.1_macOS-MountainLion -i "+args.output+"_PhyML_data.txt -p -b 0 -d nt -f e -t e -v 0.0 -o tlr -c 1 ")
	
	fileTrace=open(args.output+"_PhyML_data.txt_phyml_stats.txt")
	line=fileTrace.readline()
	linelist=line.split()
	while len(linelist)<2 or linelist[1]!="Transition/transversion":
		line=fileTrace.readline()
		linelist=line.split()
	kappaNew=float(linelist[3])
	print(kappaNew)
	line=fileTrace.readline()
	piNew=[]
	for i in range(4):
		line=fileTrace.readline()
		linelist=line.split()
		piNew.append(float(linelist[2]))
	print(piNew)
	fileTrace.close()
	
	fileTrace=open(args.output+"_PhyML_data.txt_phyml_tree.txt")
	line=fileTrace.readline()
	lineNew=splitTrifurcation(line.replace("\n",""))
	treeNew=dendropy.Tree.get_from_string(lineNew,schema="newick")
	fileTrace.close()
	for node in treeNew.leaf_nodes():
		newName=node.taxon.label.replace("'","").replace("\'","").replace(" ","_")
		node.Seq=seqs[newName]
	
	return treeNew, kappaNew, piNew
	
	
	

	


#Infer equilibrium/root frequencies and tree and subs model using FastTree v2.1.10 SSE3
def runFastTree(tree, modelType="JC"):
	if modelType=="LG":
		mSeq="-lg"
	else:
		mSeq="-gtr -nt"
	seqs=writePhylipAl(tree,args.output+"_FastTree_data.txt", modelType=modelType)
	
	print("/Applications/FastTree/FastTree -fastest -nosupport "+mSeq+" "+args.output+"_FastTree_data.txt &> "+args.output+"_FastTree_output.txt ")
	
	seedFT=1
	repeat=True
	while repeat:
		os.system("/Applications/FastTree/FastTree -fastest -seed "+str(seedFT)+" -nosupport "+mSeq+" "+args.output+"_FastTree_data.txt &> "+args.output+"_FastTree_output.txt ")
		seedFT+=1
		print("repeating "+str(seedFT))
		fileFT=open(args.output+"_FastTree_output.txt")
		line=fileFT.readline()
		while line!="":
			if line!="\n":
				oldLine=line
			line=fileFT.readline()
		if oldLine.split()[0]!="Assertion":
			repeat=False
	
	
	kappaNew=0.0
	piNew=[]
	fileTrace=open(args.output+"_FastTree_output.txt")
	line=line=fileTrace.readline()
	while line!="" and line!="\n":
		linelist=line.split()
		if linelist[0]=="GTR":
			piNew=[float(linelist[2]),float(linelist[3]),float(linelist[4]),float(linelist[5])]
			line=fileTrace.readline()
			linelist=line.split()
			rates=[float(linelist[7]),float(linelist[8]),float(linelist[9]),float(linelist[10]),float(linelist[11]),float(linelist[12])]
			kappaNew=4*(rates[1]+rates[4])/(2.0*(rates[0]+rates[2]+rates[3]+rates[5]))
			#print "kappaNew"
			#print kappaNew
		oldLine=line
		line=fileTrace.readline()
	fileTrace.close()
	
	#lineNew=splitMultifurcation(oldLine.replace("\n",""))
	lineNew=oldLine.replace("\n","")
	treeNew=dendropy.Tree.get_from_string(lineNew,schema="newick")
	treeNew.resolve_polytomies()
	
	for node in treeNew.leaf_nodes():
		newName=node.taxon.label.replace("'","").replace("\'","").replace(" ","_")
		node.Seq=seqs[newName]
	
	return treeNew, kappaNew, piNew
	
	
	



	
	
	
	
	








# #generate random trees and run both score methods
# def birthDeathTest(n=8,b=1.0,d=0.0,ntests=10):
# 	for test in range(ntests):
# 		tree=dendropy.treesim.birth_death(ntax=n,birth_rate=b, death_rate=d)
# 		print " "+str(exhaustiveEffSeqLen(tree)[1])+" "+str(effSeqLen(tree))
# 
# #generate random trees and run only the fast score method
# def birthDeathTestDPOnly(n=80,b=1.0,d=0.0,ntests=10):
# 	for test in range(ntests):
# 		tree=dendropy.treesim.birth_death(ntax=n,birth_rate=b, death_rate=d)
# 		print str(effSeqLen(tree))

def writeLeafScores(tree,oF,leafScores, method, nResidues=4, append=False, pos=0):
	for s in leafScores:
		oF.write(str(s)+" ")
	freqs1=[0.0]*nResidues
	#leaves=tree.leaf_nodes()
	nodes=tree.postorder_node_iter()
	l=0
	for node in nodes:
		if node.is_leaf():
			#print("leaf")
			#print(node.Seq)
			#print(leafScores)
			#print(node.taxon.label.replace(" ","_"))
	#for l in range(len(leaves)):
			if append:
				freqs1[node.Seq[pos]]+=leafScores[node.taxon.label.replace(" ","_")]
			else:
				freqs1[node.Seq]+=leafScores[node.taxon.label.replace(" ","_")]
			l+=1
	tot=0.0
	for n in range(nResidues):
		tot+=freqs1[n]
	bayesian=[0.0]*nResidues
	for n in range(nResidues):
		bayesian[n]=(freqs1[n]+1.0)
	#for n in range(nResidues):
	#	freqs1[n]=freqs1[n]
	oF.write("\nScore-weigthed frequencies:\n")
	for n in range(nResidues):
		oF.write(str(freqs1[n]/tot)+" ")
	oF.write("\n")
	oF.write("Score-weigthed bayesian expected frequencies:\n")
	for n in range(nResidues):
		oF.write(str(bayesian[n]/(tot+nResidues))+" ")
	oF.write("\n")
	oF.write("Score-weigthed bayesian 95 percent confidence intervals:\n")
	CI=[]
	for n in range(nResidues):
		CI.append(stats.beta.ppf([0.025, 0.975], bayesian[n], (tot+nResidues-bayesian[n])))
		#CI.append([special.betainc(bayesian[n], (tot+4.0-bayesian[n]), 0.025),special.betainc(bayesian[n], (tot+4.0-bayesian[n]), 0.975)])
	for n in range(nResidues):
		oF.write(str(CI[n][0])+":"+str(CI[n][1])+" ")
	oF.write("\n\n\n")
	
	
	
	
		
def printResults(tree,method,oF="",F=[0.25,0.25,0.25,0.25]):
	# print("Nucleotide equillibrium frequences:")
# 	print(freqs[0])
# 	print("Q matrix:")
# 	print(qMatrix[0])
# 	print("Root nucleotide frequences:")
# 	print(pi[0])
# 	exit()
	if method==4 or method==-1:
		start = time.time()
		leafScores=calculateScoreSimulationData(tree, step=100, eps=0.01, fast=1)
		elapsedTime = time.time() - start
		print("Leaf scores with simulation approach ignoring observed nucleotides (Method -1): ")
		print(leafScores)
		treeScore=sum(leafScores[1])
		print(" total tree score: "+str(treeScore))
		print(" calculateded in "+str(elapsedTime)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(leafScores[0],leafScores[1])
			writeLeafScores(tree,oF,scoresDict, method)
		if method==-1:
			return [leafScores,elapsedTime]
	
	if method==4 or method==0:
		start = time.time()
		leafScores=calculateScoreSimulationData(tree, step=100, eps=0.01, fast=0)
		elapsedTime = time.time() - start
		print("Leaf scores with simulation approach accounting for observed nucleotides (Method 0): ")
		print(leafScores)
		print(" total tree score: "+str(sum(leafScores[1])))
		print(" calculateded in "+str(elapsedTime)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(leafScores[0],leafScores[1])
			writeLeafScores(tree,oF,scoresDict, method)
		if method==0:
			return [leafScores,elapsedTime]
	
	
	if method==4 or method==1:
		start = time.time()
		leafScores2,names2,histo2,meanEsl2=exhaustiveEffSeqLenLeavesData(tree)
		elapsedTime2 = time.time() - start
		print("Leaf scores with exhaustive approach accounting for observed nucleotides (Method 1): ")
		print(leafScores2)
		print(" total tree score: "+str(meanEsl2))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,leafScores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method==1:
			return [leafScores2,elapsedTime2]
	
	
	if method==4 or method==2:
		start = time.time()
		treeScore=effSeqLenData(tree)
		print(" total fast only-tree score (from Method 2) conditional on data: "+str(treeScore))
		elapsedTime = time.time() - start
		print(" calculateded in "+str(elapsedTime)+"\n")
		if method==2:
			return [treeScore,elapsedTime]
	
	
	if method==4 or method==3:
		print("Leaf scores with efficient approach, conditional on data (Method 3): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesData(tree,F=F)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method==3:
			return [scores2,elapsedTime2]
			
	if method==4 or method==7:
		print("Leaf scores with efficient approach, not conditional on data (Method 7): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesData(tree,noData=True,F=F)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method==7:
			return [scores2,elapsedTime2]
			
	if method==4 or method==8:
		print("Leaf scores with efficient approach, conditional on data and uniform frequencies (Method 8): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesData(tree)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method==8:
			return [scores2,elapsedTime2]
			
	if method==4 or method==9:
		print("Leaf scores with efficient approach, not conditional on data and uniform frequencies (Method 9): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesData(tree,noData=True)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method==9:
			return [scores2,elapsedTime2]
				
	if method==5:
		print("Nucleotide frequencies using BEAST2: ")
		start2 = time.time()
		freqsBeast=runBEAST2(tree)
		elapsedTime2 = time.time() - start2
		print(freqsBeast)
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nFrequencies:\n")
			for i in range(4):
				oF.write(str(freqsBeast[i])+" ")
			oF.write("\n\n\n")
			#writeLeafScores(tree,oF,scores2, method)
		return [freqsBeast,elapsedTime2]
	
	if method==6:
		print("Nucleotide frequencies using PhyML: ")#PAML
		start2 = time.time()
		freqsPhyML=runPhyML(tree)
		elapsedTime2 = time.time() - start2
		print(freqsPhyML)
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nFrequencies:\n")
			for i in range(4):
				oF.write(str(freqsPhyML[i])+" ")
			oF.write("\n\n\n")
			#writeLeafScores(tree,oF,scores2, method)
		return [freqsPhyML,elapsedTime2]
		
	if method==4 or method==10:
		print("Leaf scores with approximate approach (fastPNS), conditional on data (Method 10): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesDataFast(tree, F=F)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method!=4:
			return [scores2,elapsedTime2]
	
	if method==4 or method==11:
		print("Leaf scores with approximate approach (fastPNS), conditional on data and uniform frequencies (Method 11): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesDataFast(tree)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method!=4:
			return [scores2,elapsedTime2]
			
	if method==4 or method==12:
		print("Leaf scores with approximate approach (fastPNS), not conditional on data and uniform frequencies (Method 12): ")
		start2 = time.time()
		names2,scores2=effSeqLenLeavesDataFast(tree,noData=True)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method!=4:
			return [scores2,elapsedTime2]
			
	if method==4 or method==13:
		print("Leaf scores with GSC 1994 approach (Method 13): ")
		start2 = time.time()
		names2,scores2=GSCweights(tree)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			scoresDict=listsToDictionary(names2,scores2)
			writeLeafScores(tree,oF,scoresDict, method)
		if method!=4:
			return [scores2,elapsedTime2]	
			
	if method==4 or method==15:
		print("Leaf scores with HH 1994 approach (Method 14): ")
		start2 = time.time()
		nodes=tree.postorder_node_iter()
		al=[]
		for node in nodes:
			if node.is_leaf():
		#for l in range(len(leaves)):
				al.append(node.Seq)
		scores2=HH94weights(al)
		elapsedTime2 = time.time() - start2
		print(scores2)
		print(" total tree score: "+str(sum(scores2)))
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			#scoresDict=listsToDictionary(scores2[0],scores2[1])
			writeLeafScores(tree,oF,scores2, method)
		if method!=4:
			return [scores2,elapsedTime2]	
	
	
def listsToDictionary(list1,list2):
	dic={}
	if len(list1)!=len(list2):
		print("problem with list lengths while creating dictionary")
		print(len(list1))
		print(len(list2))
	for i in range(len(list1)):
		dic[list1[i]]=list2[i]
	return dic
	


##AAs
def printResultsGenes(tree,method,treeNew, kappaNew, piNew,oF="", modelType="JC", noSequences=False):
	if method==8 or method==9 or method==11 or method==12  or method==13  or method==14  or method==15:
		print("Leaf scores with efficient approach (uniform freqs) (conditional on data :"+str(method==8 or method==11)+") (Method "+str(method)+"): ")
		start2 = time.time()
		if modelType=="LG":
			nResidues=20
		else:
			nResidues=4
		#treeNew, kappaNew, piNew=runPhyMLtree(tree)
		#elapsedTime2 = time.time() - start2
		#treeNew.reroot_at_edge(treeNew.seed_node.child_nodes()[0].edge, update_bipartitions=False)
		#treeNew.reroot_at_midpoint(update_bipartitions=False)
		#print(treeNew.seed_node.child_nodes())
		#exit()
		
		#setProbs(treeNew,kappaNew)
		#names=[]
		scores=[]
		if method==15:
			al=[]
			names2=[]
			nodes=treeNew.postorder_node_iter()
			for node in nodes:
				if node.label==None:
					node.label=""
				if node.is_leaf():
					names2.append(node.taxon.label.replace(" ","_"))
					al.append(node.Seq)
			scores2=HH94weights(al)
			dic=listsToDictionary(names2,scores2)
			scores.append(dic)
			print("\n\n weights HH94")
			print(dic)
			#names.append(names2)
			#for iN in range(len(names2)):
			#	print names2[iN]
			#	print scores2[iN]

		if method==14:
			#calculateScoreSimulationData(tree, step=100, eps=0.01, fast=2, F=[0.25,0.25,0.25,0.25], k=1.0)
			names2,scores2=calculateScoreSimulationData(treeNew, step=10, eps=0.05, fast=1, F=piNew, k=kappaNew, modelType=modelType)
			dic=listsToDictionary(names2,scores2)
			scores.append(dic)
			#print "\n\n weights 14"
			#print dic
			#names.append(names2)
			#scores.append(scores2)
		if method==13:
			names2,scores2=GSCweights(treeNew)
			dic=listsToDictionary(names2,scores2)
			scores.append(dic)
			print("\n\n weights GSC")
			print(dic)
			#names.append(names2)
			#scores.append(scores2)
			#print "\n\n weights GSC"
			#for iN in range(len(names2)):
			#	print names2[iN]
			#	print scores2[iN]
			#print(scores2)
			#print(" total tree score: "+str(sum(scores2)))
		if method==9:
			names2,scores2=effSeqLenLeavesData(treeNew, noData=True, F=piNew, fast=True, k=kappaNew, modelType=modelType)
			dic=listsToDictionary(names2,scores2)
			scores.append(dic)
			print("\n\n weights 9, PNS not conditional on data")
			print(dic)
			#names.append(names2)
			#scores.append(scores2)
			#print "\n\n weights w_s"
			#for iN in range(len(names2)):
			#	print names2[iN]
			#	print scores2[iN]
			#print(scores2)
			#print(" total tree score: "+str(sum(scores2)))
		if method==8:
			for p in range(args.geneLength):
				names2,scores2=effSeqLenLeavesData(treeNew, F=piNew, fast=True, pos=p, k=kappaNew, modelType=modelType)
				dic=listsToDictionary(names2,scores2)
				scores.append(dic)
				#names.append(names2)
				#scores.append(scores2)
				#print(scores2)
				#print(" total tree score: "+str(sum(scores2)))
		if method==12:
			names2,scores2=effSeqLenLeavesDataFast(treeNew, noData=True, F=piNew, fast=True, k=kappaNew, modelType=modelType)
			dic=listsToDictionary(names2,scores2)
			scores.append(dic)
			print("\n\n weights 12, approximate PNS approach not conditional on data")
			print(dic)
			#names.append(names2)
			#scores.append(scores2)
			#print(scores2)
			#print(" total tree score: "+str(sum(scores2)))
		if method==11:
			for p in range(args.geneLength):
				names2,scores2=effSeqLenLeavesDataFast(treeNew, F=piNew, fast=True, pos=p, k=kappaNew, modelType=modelType)
				dic=listsToDictionary(names2,scores2)
				scores.append(dic)
				#names.append(names2)
				#scores.append(scores2)
				#print(scores2)
				#print(" total tree score: "+str(sum(scores2)))
		elapsedTime2 = time.time() - start2
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nLeafScores:\n")
			if not noSequences:
				for p in range(args.geneLength):
					if method==8 or method==11:
						writeLeafScores(treeNew,oF,scores[p], method,nResidues=nResidues,append=True, pos=p)
					if method==9 or method==12 or method==13 or method==14 or method==15:
						writeLeafScores(treeNew,oF,scores[0], method,nResidues=nResidues,append=True, pos=p)
					#if method==11:
					#	writeLeafScores(treeNew,oF,scores[p], method,append=True, pos=p)
					#if method==12:
					#	writeLeafScores(treeNew,oF,scores[0], method,append=True, pos=p)
					#if method==13:
					#	writeLeafScores(treeNew,oF,scores[0], method,append=True, pos=p)
		return [scores,elapsedTime2]
			
	if method==5:
		print("Tree and nucleotide frequencies using BEAST2: ")
		start2 = time.time()
		freqsBeast=runBEAST2gene(tree)
		elapsedTime2 = time.time() - start2
		#print(freqsBeast)
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nFrequencies:\n")
			for p in range(args.geneLength):
				for i in range(4):
					oF.write(str(freqsBeast[p][i])+" ")
				oF.write("\n")
			oF.write("\n\n\n")
			#writeLeafScores(tree,oF,scores2, method)
		return [freqsBeast,elapsedTime2]
	
	if method==6:
		print("Tree and nucleotide frequencies using PhyML: ")#PAML
		start2 = time.time()
		#treeNew, kappaNew, piNew=runPhyMLtree(tree)
		freqsPhyML=[]
		for p in range(args.geneLength):
			freqsPhyML.append(runPhyML(treeNew, pos=p, kappaML=kappaNew, append=True))
		elapsedTime2 = time.time() - start2
		#print(freqsPhyML)
		print(" calculateded in "+str(elapsedTime2)+" seconds\n")
		if oF!="":
			oF.write("Method "+str(method)+", time:\n"+str(elapsedTime2)+"\nFrequencies:\n")
			for p in range(args.geneLength):
				for i in range(4):
					oF.write(str(freqsPhyML[p][i])+" ")
				oF.write("\n")
			oF.write("\n\n\n")
			#writeLeafScores(tree,oF,scores2, method)
		return [freqsPhyML,elapsedTime2]
	





#generate random trees and run both score methods, and also calculate scores for leaves
def birthDeathTestLeaves(n,b,d,ntests):
	for test in range(ntests):
		if args.simulateFreq:
			pi[0]=np.random.dirichlet(alpha=[args.dirichlet,args.dirichlet,args.dirichlet,args.dirichlet], size=None)
			freqs[0]=pi[0]
			if modelType=="HKY":
				model = dendropy.model.discrete.Hky85(kappa=1.0, base_freqs=freqs[0], state_alphabet=None, rng=None)
				qMatrix[0]=model.qmatrix(rate=1.0)
			else:
				rates=[0.1,0.4,0.15,0.09,0.42,0.13]
				qMatrix2=[[0.0,rates[0]*freqs[0][1],rates[1]*freqs[0][2],rates[2]*freqs[0][3]], [rates[0]*freqs[0][0],0.0,rates[3]*freqs[0][2],rates[4]*freqs[0][3]], [rates[1]*freqs[0][0],rates[3]*freqs[0][1],0.0,rates[5]*freqs[0][3]], [rates[2]*freqs[0][0],rates[4]*freqs[0][1],rates[5]*freqs[0][2],0.0]]
				qMatrix2[0][0]=-(rates[0]*freqs[0][1]+rates[1]*freqs[0][2]+rates[2]*freqs[0][3])
				qMatrix2[1][1]=-(rates[0]*freqs[0][0]+rates[3]*freqs[0][2]+rates[4]*freqs[0][3])
				qMatrix2[2][2]=-(rates[1]*freqs[0][0]+rates[3]*freqs[0][1]+rates[5]*freqs[0][3])
				qMatrix2[3][3]=-(rates[2]*freqs[0][0]+rates[4]*freqs[0][1]+rates[5]*freqs[0][2])
				totRate=-freqs[0][0]*qMatrix2[0][0]-freqs[0][1]*qMatrix2[1][1]-freqs[0][2]*qMatrix2[2][2]-freqs[0][3]*qMatrix2[3][3]
				for i in range(4):
					for j in range(4):
						qMatrix2[i][j]=qMatrix2[i][j]/totRate
				qMatrix[0]=qMatrix2
			
			print("Nucleotide equillibrium frequences:")
			print(freqs[0])
			print("Q matrix:")
			print(qMatrix[0])
			print("Root nucleotide frequences:")
			print(pi[0])
		oFile.write("\n\n\nSimulated frequencies:\n"+str(pi[0][0])+" "+str(pi[0][1])+" "+str(pi[0][2])+" "+str(pi[0][3])+"\n")
		
		if args.conditionalTree=="":
			print("\n Simulating new tree")
			start = time.time()
			#tree = dendropy.simulate.treesim.birth_death_tree(num_extant_tips=n,birth_rate=b, death_rate=d)
			taxaNames=[]
			for i in range(n):
				taxaNames.append("T"+str(i+1))
			taxa = dendropy.TaxonNamespace(taxaNames)
			tree = dendropy.simulate.treesim.pure_kingman_tree(taxon_namespace=taxa, pop_size=args.scale)
			#tree=dendropy.treesim.birth_death(ntax=n,birth_rate=b, death_rate=d)
			elapsedTime = time.time() - start
			print("new tree:   "+tree.as_string(schema="newick",)+" simulated in "+str(elapsedTime)+" seconds")
			oFile.write("Tree:\n"+tree.as_string(schema="newick",)+"\n")
		else:
			treestring=args.conditionalTree
			tree=dendropy.Tree.get_from_string(treestring,schema="newick")
			for node in tree.postorder_node_iter():
				if node!=tree.seed_node:
					node.edge_length=node.edge_length*args.scale
			print("Command line input tree: "+tree.as_string(schema="newick",))
			oFile.write("Tree:\n"+tree.as_string(schema="newick",)+"\n")
		if args.simulateSeq:
			start = time.time()
			simulateSeq(tree)
			elapsedTime = time.time() - start
			print("Alignment column simulated in "+str(elapsedTime)+" seconds")
			counts=[0,0,0,0]
			leaves=tree.leaf_nodes()
			for l in leaves:
				counts[l.Seq]+=1
			freqsObs=[]
			for nNuc in range(4):
				freqsObs.append(float(counts[nNuc])/len(leaves))
			oFile.write("Frquencies from nucleotide count:\n"+str(freqsObs[0])+" "+str(freqsObs[1])+" "+str(freqsObs[2])+" "+str(freqsObs[3])+" "+"\n")
			oFile.write("Nucleotide counts among leaves:\n"+str(counts[0])+" "+str(counts[1])+" "+str(counts[2])+" "+str(counts[3])+" "+"\n")
		else:
			for l in tree.leaf_nodes():
				l.Seq=-1
		columnStr=""
		for l in tree.leaf_nodes():
			columnStr+=str(l.Seq)+", "
		print(columnStr)
		
		methods=[-1,0,1,3]
		if args.noSlowMethods:
			methods=[-1,0,3]
		if args.noMediumMethods:
			methods=[3,7,8,9] #789 are respectively with uniform frequencies, without data, and without data and with uniform frequencies
		if args.fastMethods:
			methods=[3]
		if args.runBEAST2:
			methods.append(5)
		if args.runPhyML:#runPAML
			methods.append(6)
		methods.append(10)
		methods.append(13)
		for method in methods:
			printResults(tree,method,oFile,F=freqsObs)





#square branch lengths to get more unbalanced tree
def squareTree(tree):
	totLen=0.0
	totLenSq=0.0
	for node in tree.postorder_node_iter():
		if (type(node.edge_length) is float):
			totLen+=node.edge_length
			totLenSq+=node.edge_length * node.edge_length
	print("totLen: "+str(totLen))
	print("totLenSq: "+str(totLenSq))
	for node in tree.postorder_node_iter():
		if (type(node.edge_length) is float):
			node.edge_length = node.edge_length * node.edge_length * totLen/totLenSq
	return


#generate random trees and run both score methods, and also calculate scores for leaves
def simulationsGenes(ntests, modelType="JC", noSequences=False):
	if modelType=="LG":
		nResidues=20
		backgroundAA=[0.079066,0.055941,0.041977,0.053052,0.012937,0.040767,0.071586,0.057337,0.022355,0.062157,0.099081,0.0646,0.022951,0.042302,0.04404,0.061197,0.053287,0.012066,0.034155,0.069147]
		sum1=sum(backgroundAA)
		for i in range(20):
			backgroundAA[i]=backgroundAA[i]/sum1
	else:
		nResidues=4
	#if False:
	treestring=args.conditionalTree
	nHum=args.humans
	if nHum>1:
		taxaNames=[]
		for i in range(nHum):
			taxaNames.append("T"+str(i+1))
		taxa = dendropy.TaxonNamespace(taxaNames)
		start = time.time()
		tree = dendropy.simulate.treesim.pure_kingman_tree(taxon_namespace=taxa, pop_size=1)
		elapsedTime = time.time() - start
		print("Time to run coalescent: "+str(elapsedTime))
		for node in tree.postorder_node_iter():
			if node!=tree.seed_node:
				node.edge_length=node.edge_length*0.001
		#dendropy.treesim.pure_kingman(taxon_set, pop_size=1, rng=None)
		#dendropy.treesim.mean_kingman(taxon_set, pop_size=1)
		treeS=tree.as_string(schema="newick",).replace(":0.0;\n","")
		tree2=treestring.replace("Human",treeS)
		treestring=tree2
	for test in range(ntests):
		print("replicate "+str(test+1))
		# treestring=args.conditionalTree
# 		nHum=args.humans
# 		if nHum>1:
# 		
# 			taxaNames=[]
# 			for i in range(nHum):
# 				taxaNames.append("T"+str(i+1))
# 			taxa = dendropy.TaxonNamespace(taxaNames)
# 			start = time.time()
# 			tree = dendropy.simulate.treesim.pure_kingman_tree(taxon_namespace=taxa, pop_size=1)
# 			elapsedTime = time.time() - start
# 			print "Time to run coalescent: "+str(elapsedTime)
# 			for node in tree.postorder_node_iter():
# 				if node!=tree.seed_node:
# 					node.edge_length=node.edge_length*0.001
# 			#dendropy.treesim.pure_kingman(taxon_set, pop_size=1, rng=None)
# 			#dendropy.treesim.mean_kingman(taxon_set, pop_size=1)
# 			treeS=tree.as_string(schema="newick",).replace(":0.0;\n","")
# 			tree2=treestring.replace("Human",treeS)
# 			treestring=tree2
		
		tree=dendropy.Tree.get_from_string(treestring,schema="newick")
		if args.fourth:
			squareTree(tree)
			squareTree(tree)
		elif args.square:
			squareTree(tree)
		for node in tree.postorder_node_iter():
			if node!=tree.seed_node:
				node.edge_length=node.edge_length*args.scale
		#print("Command line input tree: "+tree.as_string(schema="newick",))
		oFile.write("Tree:\n"+tree.as_string(schema="newick",)+"\n")
		print("Finished creating tree ")
		leaves=tree.leaf_nodes()
		for l in leaves:
			l.Seq=[]
		#freqs=[]
		for p in range(args.geneLength):
			freqs.append(0)
			pi.append(0)
			qMatrix.append(0)
		oFile.write("\n\n\n\n")
		counts=[]
		freqsObs=[]
		if not noSequences:
			
			print("Running sequence simulation")
			start = time.time()
			for p in range(args.geneLength):
				if p>=args.geneLength*0.8:
					pi2=np.random.dirichlet(alpha=[args.dirichlet]*nResidues, size=None)
				else:
					if modelType=="LG":
						pi2=backgroundAA
					else:
						pi2=args.backgroundFreq
				freqs[p]=pi2
				pi[p]=pi2
				if modelType=="HKY":
					model = dendropy.model.discrete.Hky85(kappa=1.0, base_freqs=freqs[p], state_alphabet=None, rng=None)
					qMatrix[p]=model.qmatrix(rate=1.0)
				elif modelType=="LG":
					qMatrix[p]=LGqM(freqs[p])
				else:
					rates=[0.1,0.4,0.15,0.09,0.42,0.13]
					qMatrix2=[[0.0,rates[0]*freqs[p][1],rates[1]*freqs[p][2],rates[2]*freqs[p][3]], [rates[0]*freqs[p][0],0.0,rates[3]*freqs[p][2],rates[4]*freqs[p][3]], [rates[1]*freqs[p][0],rates[3]*freqs[p][1],0.0,rates[5]*freqs[p][3]], [rates[2]*freqs[p][0],rates[4]*freqs[p][1],rates[5]*freqs[p][2],0.0]]
					qMatrix2[0][0]=-(rates[0]*freqs[p][1]+rates[1]*freqs[p][2]+rates[2]*freqs[p][3])
					qMatrix2[1][1]=-(rates[0]*freqs[p][0]+rates[3]*freqs[p][2]+rates[4]*freqs[p][3])
					qMatrix2[2][2]=-(rates[1]*freqs[p][0]+rates[3]*freqs[p][1]+rates[5]*freqs[p][3])
					qMatrix2[3][3]=-(rates[2]*freqs[p][0]+rates[4]*freqs[p][1]+rates[5]*freqs[p][2])
					totRate=-freqs[p][0]*qMatrix2[0][0]-freqs[p][1]*qMatrix2[1][1]-freqs[p][2]*qMatrix2[2][2]-freqs[p][3]*qMatrix2[3][3]
					for i in range(4):
						for j in range(4):
							qMatrix2[i][j]=qMatrix2[i][j]/totRate
					qMatrix[p]=qMatrix2
			
				#print("Nucleotide equillibrium frequences:")
				#print(freqs[p])

				simulateSeq(tree, append=True, pos=p, modelType=modelType, k=1.0)
			
				#print("Alignment column simulated in "+str(elapsedTime)+" seconds")
				counts.append([0]*nResidues)
				#leaves=tree.leaf_nodes()
				for l in leaves:
					counts[p][l.Seq[p]]+=1
				freqsObs.append([])
				for nNuc in range(nResidues):
					freqsObs[p].append(float(counts[p][nNuc])/len(leaves))
			elapsedTime = time.time() - start
			print("Finished sequence simulation in "+str(elapsedTime)+" seconds")
		
			start2 = time.time()
			print("running fasttree")
			if nHum<=1000 or test==0:
				treeNew, kappaNew, piNew=runFastTree(tree, modelType=modelType)
			if modelType=="LG":
				piNew=backgroundAA
			elapsedTime2 = time.time() - start2
			print("finished fasttree (if run), "+str(elapsedTime2)+" seconds")
			oFile.write("\n\n"+"Running time FastTree:\n"+str(elapsedTime2)+"\n")
		
			oFile.write("\n\n"+"Simulated frequencies:\n")
			for p in range(args.geneLength):
				for i in range(nResidues):
					oFile.write(str(freqs[p][i])+" ")
				oFile.write("\n")
			oFile.write("\n\n"+"Frquencies from nucleotide count:\n")
			for p in range(args.geneLength):
				for i in range(nResidues):
					oFile.write(str(freqsObs[p][i])+" ")
				oFile.write("\n")
			oFile.write("\n\n"+"Nucleotide counts among leaves:\n")
			for p in range(args.geneLength):
				for i in range(nResidues):
					oFile.write(str(counts[p][i])+" ")
				oFile.write("\n")
			
		if modelType=="LG":
			methods=[15,14,9,11,12,13]
		else:
			if nHum>=10000:
				methods=[15,12,13]
			elif nHum>1:
				methods=[15,9,12,13]
			else:
				methods=[15,14,8,9,11,12,13,6]
				methods=[15,9,12,13]
		for method in methods:
			print("running method "+str(method))
			if not noSequences:
				printResultsGenes(tree,method,treeNew, kappaNew, piNew,oFile, modelType=modelType, noSequences=noSequences)
			else:
				if modelType=="LG":
					pi2=backgroundAA
				else:
					pi2=args.backgroundFreq
				printResultsGenes(tree,method,tree, 1.0, pi2,oFile, modelType=modelType, noSequences=noSequences)
			print("finished method "+str(method))






if __name__ == "__main__":

	

	parser = argparse.ArgumentParser(description='Calculate Phylogenetic Novelty Scores or Effective Number of Sequences for a given tree, or simulate trees and calculate the scores for them.')
	parser.add_argument("-o",'--output',default="", help='Output file where to store results.')
	parser.add_argument("-t",'--tree',default="", help='the newick-format tree for which to run the methods.')
	parser.add_argument("-s","--simulate", help="run simulations", action="store_true")
	parser.add_argument("-S","--simulateSeq", help="if running simulations, also simulate sequences at the tips", action="store_true")
	parser.add_argument("--simulateFreq", help="if running simulations, also simulate nucleotide frequencies (the same frequencies are here used at root and as equilibrium)", action="store_true")
	#parser.add_argument('--treeLengths', nargs='+', help='If doing simulations, list here the values that you want to use to scale the simulated trees. E.g. 0.1 1.0 10.0 will scale the trees first by 0.1, then by 1.0, then by 10.0', type=float)
	parser.add_argument('--conditionalTree',default="", help='If you want to run simulations but with a fixed input tree, specify here the newick-format tree to use.')
	parser.add_argument('-c','--dictionaryData', help="a dictionary where each leaf name in the input tree is associated with a nucleotide: A=0, C=1, G=2, T=3, N=-1", type=json.loads, default=None)
	parser.add_argument("--seed", help="seed for random simulator in genes simulations", type=int, default=2)
	parser.add_argument("--humans", help="number of humans to include in the tree (default 1)", type=int, default=1)
	parser.add_argument("-l","--nLeaves", help="number of leaves in simulated trees", type=int, default=6)
	parser.add_argument("--scale", help="scale simulated trees by this amount", type=float, default=1.0)
	parser.add_argument("-b","--birthRate", help="birth rate in simulated trees", type=float, default=1.0)
	parser.add_argument("--dirichlet", help="dirichlet prior parameter", type=float, default=1.0)
	parser.add_argument("-d","--deathRate", help="death rate in simulated trees", type=float, default=0.0)
	parser.add_argument("-n","--ntests", help="number of replicate trees to simulate", type=int, default=10)
	parser.add_argument("--step", help="number of simulation replicates used in method 0 (default 100)", type=int, default=100)
	parser.add_argument("-e","--epsilon", help="differential threshold to stop method 0 (lower means more accurate, default=0.01)", type=float, default=0.01)
	parser.add_argument("--method", help="which method to run: -1 = simulation-based without data; 0 = simulation-based conditional on data; 1 = brute force (very slow) ; 2 = pruning method for Effective Number of Sequences ; 3 = pruning method for Phylogenetic Novelty Scores ; 4 = all methods. (default 4)", type=int, default=4)
	parser.add_argument("--noSlowMethods", help="if running simulations, do not use the slowest methods (method 1)", action="store_true")
	parser.add_argument("--noMediumMethods", help="if running simulations, do not use the slowest anmd medium methods (method -1, 0 and 1)", action="store_true")
	parser.add_argument("--fastMethods", help="if running simulations, only use the fast methods (method 3)", action="store_true")
	parser.add_argument("--runBEAST2", help="if running simulations, use BEAST v2.5 to infer nucleotide frequencies", action="store_true")
	#parser.add_argument("--runPAML", help="if running simulations, use PAML to infer nucleotide frequencies", action="store_true")
	parser.add_argument("--runPhyML", help="if running simulations, use PhyML v3.1 to infer nucleotide frequencies", action="store_true")
	parser.add_argument("--simulateGenes", help="Simulate, and infer, entire genes", action="store_true")
	parser.add_argument("--geneLength", help="length of genes (default 100)", type=int, default=100)
	parser.add_argument("--backgroundFreq", help="background equilibrium nucleotide frequencies for sites not under selection (default A=0.3 C=0.2 G=0.2 T=0.3)", nargs=4, type=float, default=[0.3,0.2,0.2,0.3])
	parser.add_argument("-m","--model", help="substitution model, can by HKY, JC, GTR or `LG", default="JC", choices=['JC', 'HKY', 'GTR', 'LG'])
	parser.add_argument("-f","--frequencies", help="equilibrium nucleotide frequencies (only used with HKY and GTR models)", nargs=4, type=float, default=[0.25,0.25,0.25,0.25])
	parser.add_argument("--fAA", help="equilibrium aminoacid frequencies (used with LG model)", nargs=20, type=float, default=[0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05])
	parser.add_argument("-p","--rootFrequencies", help="root nucleotide frequencies (only used with HKY and GTR models)", nargs=4, type=float, default=[0.25,0.25,0.25,0.25])
	parser.add_argument("--pAA", help="root aminoacid frequencies (used with LG model)", nargs=20, type=float, default=[0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05])
	parser.add_argument("-k","--kappa", help="kappa rate for the HKY model (only used if the HKY model is selected)", type=float, default=1.0)
	parser.add_argument("-r","--rates", help="rates of the GTR moel (only used if the GTR model is selected). These are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C. The order of the rates is AC, AG, AT, CG, CT, GT.", nargs=6, type=float, default=[0.1,0.4,0.15,0.09,0.42,0.13])
	parser.add_argument("--square", help="square branch lengths", action="store_true")
	parser.add_argument("--fourth", help="fourth power branch lengths", action="store_true")
	#parser.add_argument("--weird", help="weird tree", action="store_true")
	parser.add_argument("--noSequences", help="run calculations on the real trees without simulating the sequences", action="store_true")
	args = parser.parse_args()

	oFile=open(args.output,"w")

	freqs=[args.frequencies]
	s=sum(freqs[0])
	if s!=1.0:
		for i in range(4):
			freqs[0][i]=freqs[0][i]/s
		
	pi=[args.rootFrequencies]
	s2=sum(pi[0])
	if s2!=1.0:
		for i in range(4):
			pi[0][i]=pi[0][i]/s2
			
	modelType=args.model
	
	


	#if modelType=="GTR":
	#	model = dendropy.model.discrete.NucleotideCharacterEvolutionModel(base_freqs=[0.25,0.25,0.25,0.25], state_alphabet=None, rng=None)
	if modelType=="JC":
		model = dendropy.model.discrete.Jc69(state_alphabet=None, rng=None)
		qMatrix=[model.qmatrix(rate=1.0)]
		freqs[0]=[0.25,0.25,0.25,0.25]
		pi[0]=[0.25,0.25,0.25,0.25]
	elif modelType=="HKY":
		model = dendropy.model.discrete.Hky85(kappa=args.kappa, base_freqs=freqs[0], state_alphabet=None, rng=None)
		qMatrix=[model.qmatrix(rate=1.0)]
	elif modelType=="GTR":
		#rates of a GTR model: these are symmetrical, so the first is both r_AC and r_CA. For the Q matrix you have q_AC = r_AC * pi_C.
		# the order of the rates is AC, AG, AT, CG, CT, GT.
		rates=args.rates
		qMatrix2=[[0.0,rates[0]*freqs[0][1],rates[1]*freqs[0][2],rates[2]*freqs[0][3]], [rates[0]*freqs[0][0],0.0,rates[3]*freqs[0][2],rates[4]*freqs[0][3]], [rates[1]*freqs[0][0],rates[3]*freqs[0][1],0.0,rates[5]*freqs[0][3]], [rates[2]*freqs[0][0],rates[4]*freqs[0][1],rates[5]*freqs[0][2],0.0]]
		qMatrix2[0][0]=-(rates[0]*freqs[0][1]+rates[1]*freqs[0][2]+rates[2]*freqs[0][3])
		qMatrix2[1][1]=-(rates[0]*freqs[0][0]+rates[3]*freqs[0][2]+rates[4]*freqs[0][3])
		qMatrix2[2][2]=-(rates[1]*freqs[0][0]+rates[3]*freqs[0][1]+rates[5]*freqs[0][3])
		qMatrix2[3][3]=-(rates[2]*freqs[0][0]+rates[4]*freqs[0][1]+rates[5]*freqs[0][2])
		totRate=-freqs[0][0]*qMatrix2[0][0]-freqs[0][1]*qMatrix2[1][1]-freqs[0][2]*qMatrix2[2][2]-freqs[0][3]*qMatrix2[3][3]
		for i in range(4):
			for j in range(4):
				qMatrix2[i][j]=qMatrix2[i][j]/totRate
		qMatrix=[qMatrix2]
	elif modelType=="LG":
		AAbaseFreqs=[0.079066,0.055941,0.041977,0.053052,0.012937,0.040767,0.071586,0.057337,0.022355,0.062157,0.099081,0.0646,0.022951,0.042302,0.04404,0.061197,0.053287,0.012066,0.034155,0.069147]
		if len(args.pAA)!=20:
			AApi=List(AAbaseFreqs)
		else:
			AApi=args.pAA
		pi=[AApi]
		s2=sum(pi[0])
		if s2!=1.0:
			for i in range(20):
				pi[0][i]=pi[0][i]/s2
	
		AAfreqs=args.fAA
		if len(args.fAA)!=20:
			freqs=List(AAbaseFreqs)
		else:
			freqs=[AAfreqs]
		s=sum(freqs[0])
		if s!=1.0:
			for i in range(20):
				freqs[0][i]=freqs[0][i]/s
		qMatrix=[LGqM(freqs[0])]
		
	#rates=args.rates
	
	
	#read tree from file and print its score (calculated fast)
	if args.tree!="":
		#try:
		#	F=file(args.treeFile,'r')
		#except:
		treestring=args.tree
		tree=dendropy.Tree.get_from_string(treestring,schema="newick")
		print("Command line input tree: "+tree.as_string(schema="newick",))
		data = args.dictionaryData
		if data!=None:
			for l in tree.leaf_nodes():
				l.Seq=data[l.taxon.label]
		else:
			for l in tree.leaf_nodes():
				l.Seq=-1
		columnStr=""
		for l in tree.leaf_nodes():
			columnStr+=str(l.Seq)+", "
		print(columnStr)
		printResults(tree,args.method)
	
	#except:
	#	print >>sys.stderr,"Usage: "+sys.argv[0]+" treefile\n"
	#	print("running on simulated trees instead")
	#	birthDeathTestLeaves()
	#	exit(0)

	if args.simulate:
		print("running simulations of trees ")
		birthDeathTestLeaves(args.nLeaves,args.birthRate,args.deathRate,args.ntests)



	if args.simulateGenes:
		print("running simulations of entire genes ")
		np.random.seed(args.seed)
		if args.conditionalTree=="":
			print("Error, simulations of entire genes require an input tree with the --conditionalTree option.")
			exit()
		simulationsGenes(args.ntests, modelType=modelType, noSequences=args.noSequences)

	#birthDeathTestDPOnly()
	#print exhaustiveEffSeqLen(tree)[1]
	#birthDeathTest()



