import sys
import math
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import argparse
#import matplotlib
#print(matplotlib.__version__)
#exit()

#python /Users/demaio/Desktop/PhyloNe/simulations_summaries_genes_new.py --humans 10000

parser = argparse.ArgumentParser(description='Create plots and summaries for the simulations of genes.')
parser.add_argument("--AA", help="aminoacid alignments", action="store_true")
parser.add_argument("--humans", help="number of humans to include in the tree (default 1)", type=int, default=1)
parser.add_argument("--noSequences", help="run calculations on the real trees without simulating the sequences", action="store_true")
parser.add_argument("--weird", help="weird tree?", action="store_true")
args = parser.parse_args()

#when using squared brnach lengths
filePath="/Users/demaio/Desktop/PhyloNe/simulations_allMethods_fourth_diri"
filePath="/Users/demaio/Desktop/PhyloNe/simulations_allMethods_squared_diri"

nReps=10
scales=[0.1,0.2,1.0,5.0,10.0]
scalesN=["01","02","1","5","10"]
scalesNnew=["0.1","0.2","1","5","10"]
scales=[0.2,1.0,5.0]
scalesN=["02","1","5"]
scalesNnew=["0.2","1","5"]
nLeaves=[100]

if args.AA:
	nResidues=20
	DNA = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V']
	diriS="002"
	diri=0.02
	methods=[15,14,9,11,12,13]
	filePath="/Users/demaio/Desktop/PhyloNe/simulations_allMethods_AAs_diri"
else:
	nResidues=4
	DNA=["A","C","G","T"]
	diriS="01"
	diri=0.1
	nHum=args.humans
	if nHum>=10000:
		methods=[15,12,13]
		filePath="/Users/demaio/Desktop/PhyloNe/simulations_humans"+str(args.humans)+"_diri"
		scales=[1.0]
		scalesN=["1"]
		scalesNnew=["1"]
	elif nHum>1:
		methods=[15,9,12,13]
		filePath="/Users/demaio/Desktop/PhyloNe/simulations_humans"+str(args.humans)+"_diri"
	else:
		methods=[15,14,8,9,11,12,13,6]
		#filePath="/Users/demaio/Desktop/PhyloNe/simulations_allMethods_diri"
		filePath="/Users/demaio/Desktop/PhyloNe/simulations_humans"+str(args.humans)+"_diri"
	if args.weird:
		filePath="/Users/demaio/Desktop/PhyloNe/simulations_weirdTree2_diri"
		scales=[1.0]
		scalesN=["1"]
		scalesNnew=["1"]
#print nResidues


#nLeaves=[5]
#methods=[8,9,6,5]
#methods=[14,8,9,11,12,13,6]
#methods=[14,9,11,12,13]


phyloM=[0,0,0,0,0,0,0,1]
geneL=1000

times=[]
errors2=[]
errorsB2=[]
entropies2=[]
entropiesB2=[]
entropiesErr2=[]
entropiesErrB2=[]
calibrations2=[]
errorObs2=[]
entropiesObs2=[]
entropiesObsCorr2=[]
entropiesErrObs2=[]
entropiesErrObsCorr2=[]
errorHH2=[]
entropiesHH2=[]
entropiesHHCorr2=[]
entropiesErrHH2=[]
entropiesErrHHCorr2=[]

errors1=[]
errorsB1=[]
entropies1=[]
entropiesB1=[]
entropiesErr1=[]
entropiesErrB1=[]
calibrations1=[]
errorObs1=[]
entropiesObs1=[]
entropiesObsCorr1=[]
entropiesErrObs1=[]
entropiesErrObsCorr1=[]
errorHH1=[]
entropiesHH1=[]
entropiesHHCorr1=[]
entropiesErrHH1=[]
entropiesErrHHCorr1=[]

alignSeqs=200
alignSeqsSim=[]
alignSeqsObs=[]
alignSeqsMet3=[]
alignSeqsMet32=[]
alignSeqsPhy=[]


doDiri=0
if doDiri:
	nucDiri=[]
	aaDiri=[]
	n_bins=1000
	for i in range(1000000):
		pi2=np.random.dirichlet(alpha=[0.1]*4, size=None)
		nucDiri.append(pi2[0])
		pi2=np.random.dirichlet(alpha=[0.02]*20, size=None)
		aaDiri.append(pi2[0])

	fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
	# We can set the number of bins with the `bins` kwarg
	axs[0].hist(nucDiri, bins=None)
	axs[1].hist(aaDiri, bins=None)
	plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_Dirichlet_distributions")
	exit()

	


def eucDistance(scores,oldScores):
	sum=0.0
	for i in range(len(scores)):
		sum+=(scores[i]-oldScores[i])**2
	return math.sqrt(sum)
	

def consScore(freqs):
	sum=0.0
	for i in range(len(freqs)):
		if freqs[i]>0.0:
			sum+=freqs[i]*math.log(freqs[i],2)
	return math.log(len(freqs),2)+sum


def entropy(freqs):
	sum=0.0
	for i in range(len(freqs)):
		if freqs[i]>0.0:
			sum+=freqs[i]*math.log(freqs[i],2)
	return (-sum)



sumLog=[0.0]
def weightedScore(cA,cC,cG,cT):
	tot=cA+cC+cG+cT
	sum=tot*math.log(0.25)
	if tot>len(sumLog):
		last=sumLog[len(sumLog)-1]
		#print(last)
		start=len(sumLog)
		for i in range(tot-start):
			last+=math.log(float(start+i+1))
			#print(math.log(float(start+i+1)))
			#print(last)
			sumLog.append(last)
		#print(sumLog)
		#exit()
	sum+=sumLog[tot-1]
	if cA>0:
		sum-=sumLog[cA-1]
	if cC>0:
		sum-=sumLog[cC-1]
	if cG>0:
		sum-=sumLog[cG-1]
	if cT>0:
		sum-=sumLog[cT-1]
	freqs=[float(cA)/tot,float(cC)/tot,float(cG)/tot,float(cT)/tot]
	score=entropy(freqs)
	#print("contribution of "+str(cA)+" "+str(cC)+" "+str(cG)+" "+str(cT)+" "+str(sum)+" "+str(math.exp(sum))+" "+str(score)+" "+str(math.exp(sum)*score))
	return math.exp(sum)*score
	

	
doCorrection=0
if doCorrection:
	corrections={}
	for n in nLeaves:
		sum2=0.0
		nA=n
		nC=0
		nG=0
		nT=0
		done=False
		while (not done):
			sum2+=weightedScore(nA,nC,nG,nT)
			#print(str(sum2))	
			#if nT>0:
			if nG>0:
				nG-=1
				nT+=1
			elif nC>0:
				nC-=1
				nG=nT+1
				nT=0
			elif nA>0:
				nA-=1
				nC=nT+1
				nT=0
			else:
				done=True
		corrections[n]=sum2
		#print(str(n)+" "+str(sum2))
#print(sumLog)
#exit()	


def consScoreCorr(freqs,nTips):
	sum=0.0
	for i in range(len(freqs)):
		if freqs[i]>0.0:
			sum+=freqs[i]*math.log(freqs[i],2)
	return corrections[nTips]+sum


def set_axis_style(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)
    ax.set_xlabel('Method')
    
def set_axis_style2(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)
    ax.set_xlabel('Scale')


for s in range(len(scalesN)):
	times.append([])
	errors1.append([])
	entropies1.append([])
	entropiesErr1.append([])
	errorsB1.append([])
	entropiesB1.append([])
	entropiesErrB1.append([])
	calibrations1.append([])
	errorObs1.append([])
	entropiesObs1.append([])
	entropiesObsCorr1.append([])
	entropiesErrObs1.append([])
	entropiesErrObsCorr1.append([])
	errorHH1.append([])
	entropiesHH1.append([])
	entropiesHHCorr1.append([])
	entropiesErrHH1.append([])
	entropiesErrHHCorr1.append([])
	errors2.append([])
	entropies2.append([])
	entropiesErr2.append([])
	errorsB2.append([])
	entropiesB2.append([])
	entropiesErrB2.append([])
	calibrations2.append([])
	errorObs2.append([])
	entropiesObs2.append([])
	entropiesObsCorr2.append([])
	entropiesErrObs2.append([])
	entropiesErrObsCorr2.append([])
	errorHH2.append([])
	entropiesHH2.append([])
	entropiesHHCorr2.append([])
	entropiesErrHH2.append([])
	entropiesErrHHCorr2.append([])
	alignSeqsSim.append([])
	alignSeqsObs.append([])
	alignSeqsMet3.append([])
	alignSeqsMet32.append([])
	alignSeqsPhy.append([])
	for n in range(len(nLeaves)):
		times[s].append([])
		errors1[s].append([])
		entropies1[s].append([])
		entropiesErr1[s].append([])
		errorsB1[s].append([])
		entropiesB1[s].append([])
		entropiesErrB1[s].append([])
		calibrations1[s].append([])
		errorObs1[s].append([])
		entropiesObs1[s].append([])
		entropiesErrObs1[s].append([])
		entropiesObsCorr1[s].append([])
		entropiesErrObsCorr1[s].append([])
		errorHH1[s].append([])
		entropiesHH1[s].append([])
		entropiesErrHH1[s].append([])
		entropiesHHCorr1[s].append([])
		entropiesErrHHCorr1[s].append([])
		errors2[s].append([])
		entropies2[s].append([])
		entropiesErr2[s].append([])
		errorsB2[s].append([])
		entropiesB2[s].append([])
		entropiesErrB2[s].append([])
		calibrations2[s].append([])
		errorObs2[s].append([])
		entropiesObs2[s].append([])
		entropiesErrObs2[s].append([])
		entropiesObsCorr2[s].append([])
		entropiesErrObsCorr2[s].append([])
		errorHH2[s].append([])
		entropiesHH2[s].append([])
		entropiesErrHH2[s].append([])
		entropiesHHCorr2[s].append([])
		entropiesErrHHCorr2[s].append([])
		alignSeqsSim[s].append([])
		alignSeqsObs[s].append([])
		alignSeqsMet3[s].append([])
		alignSeqsMet32[s].append([])
		alignSeqsPhy[s].append([])
		for i in range(alignSeqs):
			alignSeqsSim[s][n].append("")
			alignSeqsMet3[s][n].append("")
			alignSeqsMet32[s][n].append("")
			alignSeqsPhy[s][n].append("")
		for i in range(nLeaves[n]):
			alignSeqsObs[s][n].append("")
		for m in methods:
			times[s][n].append([])
			errors1[s][n].append([])
			entropies1[s][n].append([])
			entropiesErr1[s][n].append([])
			errorsB1[s][n].append([])
			entropiesB1[s][n].append([])
			entropiesErrB1[s][n].append([])
			calibrations1[s][n].append(0)
			errors2[s][n].append([])
			entropies2[s][n].append([])
			entropiesErr2[s][n].append([])
			errorsB2[s][n].append([])
			entropiesB2[s][n].append([])
			entropiesErrB2[s][n].append([])
			calibrations2[s][n].append(0)
		times[s][n].append([])
		if args.weird:
			file=open(filePath+str(diriS)+"_"+str(nReps)+"reps_new.txt")
		else:
			file=open(filePath+str(diriS)+"_"+str(nReps)+"reps_scale"+scalesN[s]+"_new.txt")
		line=file.readline()
		linelist=line.split()
		simFreqs1=[]
		simFreqs2=[]
		simFreqs=[]
		obsFreqs=[]
		obsFreqs1=[]
		obsFreqs2=[]
		HHFreqs1=[]
		HHFreqs2=[]
		for r in range(nReps):
			line=file.readline()
			linelist=line.split()
			if not args.noSequences:
				while len(linelist)<2 or linelist[0]!="Running":
					line=file.readline()
					linelist=line.split()
				line=file.readline()
				fastTreeTime=float(line)
				while len(linelist)<2 or linelist[0]!="Simulated":
					line=file.readline()
					linelist=line.split()
				for g in range(geneL):
					line=file.readline()
					linelist=line.split()
					simFreqs.append([])
					for i in range(nResidues):
						simFreqs[geneL*r+g].append(float(linelist[i]))
					if g<geneL*0.8:
						simFreqs1.append([])
						for i in range(nResidues):
							simFreqs1[len(simFreqs1)-1].append(float(linelist[i]))
					else:
						simFreqs2.append([])
						for i in range(nResidues):
							simFreqs2[len(simFreqs2)-1].append(float(linelist[i]))
				if r==0:
					for g in range(geneL):
						nums=[]
						tot=0
						for i in range(nResidues):
							nums.append(int(math.floor(simFreqs[g][i]*alignSeqs)))
							#print nums
							for j in range(nums[i]):
								alignSeqsSim[s][n][tot+j]+=DNA[i]
							tot+=nums[i]
						for i in range(alignSeqs-tot):
							alignSeqsSim[s][n][tot+i]+="-"
				
				while len(linelist)<2 or linelist[0]!="Frquencies":
					line=file.readline()
					linelist=line.split()
				for g in range(geneL):
					line=file.readline()
					linelist=line.split()
					#if not args.noSequences:
					#HeniHeni=[0]*nResidues
					#for i in range(nResidues):
					#	if float(linelist[i])>0.00000001:
					#		HeniHeni[i]=1
					#HHfreq=1.0/sum(HeniHeni)
					#HHfreqs=[0.0]*nResidues
					#for i in range(nResidues):
					#	if HeniHeni[i]:
					#		HHfreqs[i]=HHfreq
					if r==0:
						obsFreqs.append([])
						for i in range(nResidues):
							obsFreqs[len(obsFreqs)-1].append(float(linelist[i]))
					if g<geneL*0.8:
						obsFreqs1.append([])
						for i in range(nResidues):
							obsFreqs1[len(obsFreqs1)-1].append(float(linelist[i]))
						#HHFreqs1.append(HHfreqs)
					else:
						obsFreqs2.append([])
						for i in range(nResidues):
							obsFreqs2[len(obsFreqs2)-1].append(float(linelist[i]))
						#HHFreqs2.append(HHfreqs)
			# if r==0:
# 				for g in range(geneL):
# 					nums=[]
# 					tot=0
# 					for i in range(nResidues):
# 						nums.append(int(round(obsFreqs[g][i]*nLeaves[n])))
# 						for j in range(nums[i]):
# 							alignSeqsObs[s][n][tot+j]+=DNA[i]
# 						tot+=nums[i]
# 					for i in range(nLeaves[n]-tot):
# 						alignSeqsObs[s][n][tot+i]+="-"
			if not args.noSequences:
				for g in range(int(geneL*0.8)):
					errorObs1[s][n].append(eucDistance(obsFreqs1[int(geneL*0.8)*r+g],simFreqs1[int(geneL*0.8)*r+g]))
					#errorHH1[s][n].append(eucDistance(HHFreqs1[int(geneL*0.8)*r+g],simFreqs1[int(geneL*0.8)*r+g]))
					entropiesObs1[s][n].append(consScore(obsFreqs1[int(geneL*0.8)*r+g])-consScore(simFreqs1[int(geneL*0.8)*r+g]))
					#entropiesHH1[s][n].append(consScore(HHFreqs1[int(geneL*0.8)*r+g])-consScore(simFreqs1[int(geneL*0.8)*r+g]))
					entropiesErrObs1[s][n].append(abs(consScore(obsFreqs1[int(geneL*0.8)*r+g])-consScore(simFreqs1[int(geneL*0.8)*r+g])))
					#entropiesErrHH1[s][n].append(abs(consScore(HHFreqs1[int(geneL*0.8)*r+g])-consScore(simFreqs1[int(geneL*0.8)*r+g])))
					#entropiesObsCorr1[s][n].append(consScoreCorr(obsFreqs1[int(geneL*0.8)*r+g],nLeaves[n])-consScore(simFreqs1[int(geneL*0.8)*r+g]))
					#entropiesHHCorr1[s][n].append(consScoreCorr(HHFreqs1[int(geneL*0.8)*r+g],nLeaves[n])-consScore(simFreqs1[int(geneL*0.8)*r+g]))
					#entropiesErrObsCorr1[s][n].append(abs(consScoreCorr(obsFreqs1[int(geneL*0.8)*r+g],nLeaves[n])-consScore(simFreqs1[int(geneL*0.8)*r+g])))
					#entropiesErrHHCorr1[s][n].append(abs(consScoreCorr(HHFreqs1[int(geneL*0.8)*r+g],nLeaves[n])-consScore(simFreqs1[int(geneL*0.8)*r+g])))
				for g in range(int(geneL*0.2)):
					errorObs2[s][n].append(eucDistance(obsFreqs2[int(geneL*0.2)*r+g],simFreqs2[int(geneL*0.2)*r+g]))
					#errorHH2[s][n].append(eucDistance(HHFreqs2[int(geneL*0.2)*r+g],simFreqs2[int(geneL*0.2)*r+g]))
					entropiesObs2[s][n].append(consScore(obsFreqs2[int(geneL*0.2)*r+g])-consScore(simFreqs2[int(geneL*0.2)*r+g]))
					#entropiesHH2[s][n].append(consScore(HHFreqs2[int(geneL*0.2)*r+g])-consScore(simFreqs2[int(geneL*0.2)*r+g]))
					entropiesErrObs2[s][n].append(abs(consScore(obsFreqs2[int(geneL*0.2)*r+g])-consScore(simFreqs2[int(geneL*0.2)*r+g])))
					#entropiesErrHH2[s][n].append(abs(consScore(HHFreqs2[int(geneL*0.2)*r+g])-consScore(simFreqs2[int(geneL*0.2)*r+g])))
					#entropiesObsCorr2[s][n].append(consScoreCorr(obsFreqs2[int(geneL*0.2)*r+g],nLeaves[n])-consScore(simFreqs2[int(geneL*0.2)*r+g]))
					#entropiesHHCorr2[s][n].append(consScoreCorr(HHFreqs2[int(geneL*0.2)*r+g],nLeaves[n])-consScore(simFreqs2[int(geneL*0.2)*r+g]))
					#entropiesErrObsCorr2[s][n].append(abs(consScoreCorr(obsFreqs2[int(geneL*0.2)*r+g],nLeaves[n])-consScore(simFreqs2[int(geneL*0.2)*r+g])))
					#entropiesErrHHCorr2[s][n].append(abs(consScoreCorr(HHFreqs2[int(geneL*0.2)*r+g],nLeaves[n])-consScore(simFreqs2[int(geneL*0.2)*r+g])))

			for m in range(len(methods)):
				while len(linelist)<2 or linelist[0]!="Method":
					line=file.readline()
					linelist=line.split()
				line=file.readline()
				linelist=line.split()
				times[s][n][m].append(float(linelist[0]))
				line=file.readline()
				#line=file.readline()
				
				if not args.noSequences:
				
					if phyloM[m]==1:
						for g in range(geneL):
							line=file.readline()
							linelist=line.split()
							beastFreqs=[]
							#linelist=line.split()
							for i in range(nResidues):
								beastFreqs.append(float(linelist[i]))
							
							#print("Method "+str(m)+" Freq "+str(simFreqs[geneL*r+g][0])+" "+str(simFreqs[geneL*r+g][1])+" "+str(simFreqs[geneL*r+g][2])+" "+str(simFreqs[geneL*r+g][3])+" "+" inference "+str(beastFreqs[0])+" "+str(beastFreqs[1])+" "+str(beastFreqs[2])+" "+str(beastFreqs[3])+" ")
							if g<geneL*0.8:
								errors1[s][n][m].append(eucDistance(beastFreqs,simFreqs[geneL*r+g]))
								entropies1[s][n][m].append(consScore(beastFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErr1[s][n][m].append(abs(consScore(beastFreqs)-consScore(simFreqs[geneL*r+g])))
							else:
								errors2[s][n][m].append(eucDistance(beastFreqs,simFreqs[geneL*r+g]))
								entropies2[s][n][m].append(consScore(beastFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErr2[s][n][m].append(abs(consScore(beastFreqs)-consScore(simFreqs[geneL*r+g])))
							
							if r==0 and phyloM[m]==1:
							#for g in range(100):
								#print methods
								#print phyloM
								#print m
								
								nums=[]
								tot=0
								for i in range(nResidues):
									nums.append(int(math.floor(beastFreqs[i]*alignSeqs)))
									for j in range(nums[i]):
										alignSeqsPhy[s][n][tot+j]+=DNA[i]
									tot+=nums[i]
								for i in range(alignSeqs-tot):
									alignSeqsPhy[s][n][tot+i]+="-"
				
					else:
						for g in range(geneL):
							line=file.readline()
							line=file.readline()
							line=file.readline()
							wFreqs=[]
							linelist=line.split()
							#print(linelist)
							#print(str(s)+" "+str(n)+" "+str(r)+" "+str(m)+" ")
							for i in range(nResidues):
								wFreqs.append(float(linelist[i]))
						
					
									#alignSeqsMet3[s][n][tot+i]+="-"
							#print(len(simFreqs))
							#print(r)
							#print(g)
							#print(len(simFreqs[100*r+g]))
							#print(simFreqs[100*r+g])
							#print m
							#print(wFreqs)
							#print simFreqs[geneL*r+g]
							if g<geneL*0.8:
								#print obsFreqs1[int(geneL*0.8)*r+g]
								errors1[s][n][m].append(eucDistance(wFreqs,simFreqs[geneL*r+g]))
								entropies1[s][n][m].append(consScore(wFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErr1[s][n][m].append(abs(consScore(wFreqs)-consScore(simFreqs[geneL*r+g])))
							else:
								#print obsFreqs2[int(geneL*0.2)*r+g-int(geneL*0.8)]
								errors2[s][n][m].append(eucDistance(wFreqs,simFreqs[geneL*r+g]))
								entropies2[s][n][m].append(consScore(wFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErr2[s][n][m].append(abs(consScore(wFreqs)-consScore(simFreqs[geneL*r+g])))
				
							line=file.readline()
							line=file.readline()
							bFreqs=[]
							linelist=line.split()
							for i in range(nResidues):
								bFreqs.append(float(linelist[i]))
						# if m==0 and r==0:
	# 						nums=[]
	# 						tot=0
	# 						for i in range(4):
	# 							nums.append(math.floor(bFreqs[i]*alignSeqs))
	# 							for j in range(nums[i]):
	# 								alginSeqsMet3B[s][n][tot+j]+=DNA[i]
	# 							tot+=nums[i]
	# 						for i in range(alignSeqs-tot):
	# 							alginSeqsMet3B[s][n][tot+i]+="-"
							if g<geneL*0.8:
								errorsB1[s][n][m].append(eucDistance(bFreqs,simFreqs[geneL*r+g]))
								entropiesB1[s][n][m].append(consScore(bFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErrB1[s][n][m].append(abs(consScore(bFreqs)-consScore(simFreqs[geneL*r+g])))
							else:
								errorsB2[s][n][m].append(eucDistance(bFreqs,simFreqs[geneL*r+g]))
								entropiesB2[s][n][m].append(consScore(bFreqs)-consScore(simFreqs[geneL*r+g]))
								entropiesErrB2[s][n][m].append(abs(consScore(bFreqs)-consScore(simFreqs[geneL*r+g])))
				
				
							line=file.readline()
							#print(line)
							line=file.readline()
							CIs=[]
							linelist=line.split()
							#print(line)
							#print(str(g))
							for i in range(nResidues):
								CIs.append([float(linelist[i].split(":")[0]),float(linelist[i].split(":")[1])])
							for i in range(nResidues):
								if simFreqs[geneL*r+g][i]>=CIs[i][0] and simFreqs[geneL*r+g][i]<=CIs[i][1]:
									if g<geneL*0.8:
										calibrations1[s][n][m]+=1.0/(nReps*geneL*0.8*nResidues)
									else:
										calibrations2[s][n][m]+=1.0/(nReps*geneL*0.2*nResidues)
							line=file.readline()
							line=file.readline()
						
							if r==0:
								#for g in range(100):
									nums=[]
									tot=0
									for i in range(nResidues):
										nums.append(int(math.floor(wFreqs[i]*alignSeqs)))
										for j in range(nums[i]):
											if m==0:
												alignSeqsMet3[s][n][tot+j]+=DNA[i]
											if m==1:
												alignSeqsMet32[s][n][tot+j]+=DNA[i]
										tot+=nums[i]
									for i in range(alignSeqs-tot):
										if m==0:
											alignSeqsMet3[s][n][tot+i]+="-"
										if m==1:
											alignSeqsMet32[s][n][tot+i]+="-"
										
			if not args.noSequences:
				times[s][n][-1].append(fastTreeTime)	
				
		#print("\n\n\n results with "+str(nLeaves[n])+" leaves and scale "+scalesN[s])
		#print(errorObs1[s][n])
		#print(entropiesObs1[s][n])
		#print(errorObs2[s][n])
		#print(entropiesObs2[s][n])
		#print(times[s][n])
		#print(errors1[s][n])
		#print(entropies1[s][n])
		#print(errorsB1[s][n])
		#print(entropiesB1[s][n])
		#print(calibrations1[s][n])
		#print(errors2[s][n])
		#print(entropies2[s][n])
		#print(errorsB2[s][n])
		#print(entropiesB2[s][n])
		#print(calibrations2[s][n])
				


fs = 10  # fontsize
if args.AA:
	#methods=[14,9,11,12,13]
	pos=[1,2,3,4,5,6,7]
	x = ["FastTree", "$w_s$","$w_s$ (simu.)","$\overline{w}_s$", "HH94", "GSC94", "$\overline{w}^D_s$"]
	order=[-1,2,1,4,0,5,3]
else:
	if nHum>=10000:
		#methods=[12,13]
		pos=[1,2,3,4]
		x = ["FastTree", "$\overline{w}_s$", "HH94", "GSC94"]
		order=[-1,1,0,2]
	elif nHum>1:
		#methods=[9,12,13]
		pos=[1,2,3,4,5]
		x = ["FastTree",  "$w_s$","$\overline{w}_s$", "HH94", "GSC94"]
		order=[-1,1,2,0,3]
	else:
		#methods=[14,8,9,11,12,13,6]
		pos=[1,2,3,4,5,6,7,8,9]
		x = ["FastTree", "$w_s$", "$w_s$ (simu.)","$\overline{w}_s$", "HH94", "GSC94", "$w^D_s$", "$\overline{w}^D_s$", "PhyML"]
		order=[-1,3,1,5,0,6,2,4,7]
fig, axes = plt.subplots(nrows=1, ncols=len(scalesN), figsize=(len(scalesN)*(len(pos)+1), 5))
if len(scalesN)>1:
	for s in range(len(scalesN)):
		for n in range(len(nLeaves)):
			data=[]
			for i in order:
				data.append(times[s][n][i])
			#data = [times[s][n][-1],times[s][n][2],times[s][n][0],times[s][n][4],times[s][n][5],times[s][n][1],times[s][n][3],times[s][n][6]]
			axes[s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
			axes[s].set_title('scale '+scalesNnew[s], fontsize=fs)
			axes[s].set_yscale('log')
			set_axis_style(axes[s],x)
else:
	data=[]
	for i in order:
		data.append(times[0][0][i])
	#data = [times[s][n][-1],times[s][n][2],times[s][n][0],times[s][n][4],times[s][n][5],times[s][n][1],times[s][n][3],times[s][n][6]]
	axes.violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
	axes.set_title('scale '+scalesNnew[0], fontsize=fs)
	axes.set_yscale('log')
	set_axis_style(axes,x)
fig.tight_layout()
#fig.suptitle("Time (seconds) for execution of methods")
fig.subplots_adjust(hspace=0.6)
plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_times_new2")




fs = 10  # fontsize
if args.AA:
	#methods=[14,9,11,12,13]
	pos=[1,2,3,4,5]
	x = ["FastTree", "$w_s$", "$\overline{w}_s$", "HH94", "GSC94"]
	order=[-1,2,4,0,5]
else:
	if nHum>=10000:
		#methods=[12,13]
		pos=[1,2,3,4]
		x = ["FastTree", "$\overline{w}_s$", "HH94", "GSC94"]
		order=[-1,1,0,2]
	elif nHum>1:
		#methods=[9,12,13]
		pos=[1,2,3,4,5]
		x = ["FastTree",  "$w_s$", "$\overline{w}_s$", "HH94", "GSC94"]
		order=[-1,1,2,0,3]
	else:
		#methods=[14,8,9,11,12,13,6]
		pos=[1,2,3,4,5,6,7]
		x = ["FastTree", "$w_s$", "$\overline{w}_s$", "HH94", "GSC94", "$w^D_s$", "PhyML"]
		order=[-1,3,5,0,6,2,7]
fig, axes = plt.subplots(nrows=1, ncols=len(scalesN), figsize=(len(scalesN)*(len(pos)+1), 5))
if len(scalesN)>1:
	for s in range(len(scalesN)):
		for n in range(len(nLeaves)):
			data=[]
			for i in order:
				data.append(times[s][n][i])
			#data = [times[s][n][-1],times[s][n][2],times[s][n][0],times[s][n][4],times[s][n][5],times[s][n][1],times[s][n][3],times[s][n][6]]
			axes[s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
			axes[s].set_title('scale '+scalesNnew[s], fontsize=fs)
			axes[s].set_yscale('log')
			set_axis_style(axes[s],x)
else:
	data=[]
	for i in order:
		data.append(times[0][0][i])
	#data = [times[s][n][-1],times[s][n][2],times[s][n][0],times[s][n][4],times[s][n][5],times[s][n][1],times[s][n][3],times[s][n][6]]
	axes.violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
	axes.set_title('scale '+scalesNnew[s], fontsize=fs)
	axes.set_yscale('log')
	set_axis_style(axes,x)
fig.tight_layout()
#fig.suptitle("Time (seconds) for execution of methods")
fig.subplots_adjust(hspace=0.6)
plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_times_fewerMethods_new2")


fs = 10  # fontsize
n=0
fig, axes = plt.subplots(nrows=2, ncols=len(scalesN), figsize=(len(scalesN)*18, 10))
for s in range(len(scalesN)):
	if args.AA:
		pos=[1,2,3,4,5,6,7,8,9,10,11,12]
		x = ["Observed", "HH94", "HH94-B", "GSC94", "GSC94-B", "$w_s$", "$w_s$ (simu.)", "$w_s$-B", "$\overline{w}_s$", "$\overline{w}_s$-B", "$\overline{w}_s^D$", "$\overline{w}_s^D$-B"]
		data1 = [errorObs1[s][n],errors1[s][n][0],errorsB1[s][n][0],errors1[s][n][5],errorsB1[s][n][5],errors1[s][n][2],errors1[s][n][1],errorsB1[s][n][2],errors1[s][n][4],errorsB1[s][n][4],errors1[s][n][3],errorsB1[s][n][3]]#
		data2 = [errorObs2[s][n],errors2[s][n][0],errorsB2[s][n][0],errors2[s][n][5],errorsB2[s][n][5],errors2[s][n][2],errors2[s][n][1],errorsB2[s][n][2],errors2[s][n][4],errorsB2[s][n][4],errors2[s][n][3],errorsB2[s][n][3]]
	else:
		if nHum>=10000:
			#methods=[12,13]
			pos=[1,2,3,4,5,6,7]
			x = ["Observed", "HH94", "HH94-B", "GSC94", "GSC94-B", "$\overline{w}_s$", "$\overline{w}_s$-B"]
			data1 = [errorObs1[s][n],errors1[s][n][0],errorsB1[s][n][0],errors1[s][n][2],errorsB1[s][n][2],errors1[s][n][1],errorsB1[s][n][1]]#
			data2 = [errorObs2[s][n],errors2[s][n][0],errorsB2[s][n][0],errors2[s][n][2],errorsB2[s][n][2],errors2[s][n][1],errorsB2[s][n][1]]
		elif nHum>1:
			#methods=[9,12,13]
			pos=[1,2,3,4,5,6,7,8,9]
			x = ["Observed", "HH94", "HH94-B", "GSC94", "GSC94-B", "$w_s$", "$w_s$-B", "$\overline{w}_s$", "$\overline{w}_s$-B"]
			data1 = [errorObs1[s][n],errors1[s][n][0],errorsB1[s][n][0],errors1[s][n][3],errorsB1[s][n][3],errors1[s][n][1],errorsB1[s][n][1],errors1[s][n][2],errorsB1[s][n][2]]#
			data2 = [errorObs2[s][n],errors2[s][n][0],errorsB2[s][n][0],errors2[s][n][3],errorsB2[s][n][3],errors2[s][n][1],errorsB2[s][n][1],errors2[s][n][2],errorsB2[s][n][2]]
		else:
			#methods=[14,8,9,11,12,13,6]
			pos=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
			x = ["Observed", "HH94", "HH94-B", "GSC94", "GSC94-B", "$w_s$", "$w_s$ (simu.)", "$w_s$-B", "$\overline{w}_s$", "$\overline{w}_s$-B", "$w_s^D$", "$w_s^D$-B", "$\overline{w}_s^D$", "$\overline{w}_s^D$-B","PhyML"]
			data1 = [errorObs1[s][n],errors1[s][n][0],errorsB1[s][n][0],errors1[s][n][6],errorsB1[s][n][6],errors1[s][n][3],errors1[s][n][1],errorsB1[s][n][3],errors1[s][n][5],errorsB1[s][n][5],errors1[s][n][2],errorsB1[s][n][2],errors1[s][n][4],errorsB1[s][n][4],errors1[s][n][7]]#
			data2 = [errorObs2[s][n],errors2[s][n][0],errorsB2[s][n][0],errors2[s][n][6],errorsB2[s][n][6],errors2[s][n][3],errors2[s][n][1],errorsB2[s][n][3],errors2[s][n][5],errorsB2[s][n][5],errors2[s][n][2],errorsB2[s][n][2],errors2[s][n][4],errorsB2[s][n][4],errors2[s][n][7]]

	#for n in range(len(nLeaves)):
	#data = [errorObs1[s][n],errorObs2[s][n],errors1[s][n][0],errors2[s][n][0],errorsB1[s][n][0],errorsB2[s][n][0],errors1[s][n][1],errors2[s][n][1],errorsB1[s][n][1],errorsB2[s][n][1],errors1[s][n][2],errors2[s][n][2],errors1[s][n][3],errors2[s][n][3]]#
	#data1 = [errorObs1[s][n],errorHH1[s][n],errors1[s][n][5],errorsB1[s][n][5],errors1[s][n][2],errors1[s][n][0],errorsB1[s][n][2],errors1[s][n][4],errorsB1[s][n][4],errors1[s][n][1],errorsB1[s][n][1],errors1[s][n][3],errorsB1[s][n][3],errors1[s][n][6]]#
	if len(scalesN)==1:
		axes[0].violinplot(data1, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		axes[0].axhline(y=sum(data1[0])/len(data1[0]),color="black", linestyle="--")
		set_axis_style(axes[0],x)
		
		#data2 = [errorObs2[s][n],errorHH2[s][n],errors2[s][n][5],errorsB2[s][n][5],errors2[s][n][2],errors2[s][n][0],errorsB2[s][n][2],errors2[s][n][4],errorsB2[s][n][4],errors2[s][n][1],errorsB2[s][n][1],errors2[s][n][3],errorsB2[s][n][3],errors2[s][n][6]]
		axes[1].violinplot(data2, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		#print sum(data2[0])/len(data2[0])
		axes[1].axhline(y=sum(data2[0])/len(data2[0]),color="black", linestyle="--")
		set_axis_style(axes[1],x)
	
	else:
		axes[0][s].violinplot(data1, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0][s].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		axes[0][s].axhline(y=sum(data1[0])/len(data1[0]),color="black", linestyle="--")
		#axes[0][s].hline(sum(data1[0])/len(data1[0]),colors="black", linestyle="dashed")
		set_axis_style(axes[0][s],x)
		
		#data2 = [errorObs2[s][n],errorHH2[s][n],errors2[s][n][5],errorsB2[s][n][5],errors2[s][n][2],errors2[s][n][0],errorsB2[s][n][2],errors2[s][n][4],errorsB2[s][n][4],errors2[s][n][1],errorsB2[s][n][1],errors2[s][n][3],errorsB2[s][n][3],errors2[s][n][6]]
		axes[1][s].violinplot(data2, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1][s].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		axes[1][s].axhline(y=sum(data2[0])/len(data2[0]),color="black", linestyle="--")
		#axes[1][s].hline(sum(data2[0])/len(data2[0]),colors="black", linestyle="dashed")
		set_axis_style(axes[1][s],x)
fig.tight_layout()
#fig.suptitle("Error in estimation of equilibrium frequencies")
fig.subplots_adjust(hspace=0.3)
plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_errors_new2")


fs = 10  # fontsize
if args.AA:
	pos=[1,2,3,4,5]
	x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$"]
	order=[0,5,2,4]
else:
	if nHum>=10000:
		#methods=[12,13]
		pos=[1,2,3,4]
		x = ["Observed", "HH94", "GSC94", "$\overline{w}_s$"]
		order=[0,2,1]
	elif nHum>1:
		#methods=[9,12,13]
		pos=[1,2,3,4,5]
		x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$"]
		order=[0,3,1,2]
	else:
		#methods=[14,8,9,11,12,13,6]
		pos=[1,2,3,4,5,6]
		x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$","PhyML"]
		order=[0,6,3,5,7]
fig, axes = plt.subplots(nrows=2, ncols=len(scalesN), figsize=(len(scalesN)*12, 10))
n=0
for s in range(len(scalesN)):
	#for n in range(len(nLeaves)):
	#data = [errorObs1[s][n],errorObs2[s][n],errors1[s][n][0],errors2[s][n][0],errorsB1[s][n][0],errorsB2[s][n][0],errors1[s][n][1],errors2[s][n][1],errorsB1[s][n][1],errorsB2[s][n][1],errors1[s][n][2],errors2[s][n][2],errors1[s][n][3],errors2[s][n][3]]#
	#data = [errorObs1[s][n],errorHH1[s][n],errors1[s][n][5],errors1[s][n][2],errors1[s][n][4],errors1[s][n][6]]#
	data=[errorObs1[s][n]] #,errorHH1[s][n]
	for i in order:
		data.append(errors1[s][n][i])
	if len(scalesN)==1:
		axes[0].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		axes[0].axhline(y=sum(data[0])/len(data[0]),color="black", linestyle="--")
		set_axis_style(axes[0],x)

	else:
		axes[0][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0][s].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		axes[0][s].axhline(y=sum(data[0])/len(data[0]),color="black", linestyle="--")
		set_axis_style(axes[0][s],x)
		
	#data = [errorObs2[s][n],errorHH2[s][n],errors2[s][n][5],errors2[s][n][2],errors2[s][n][4],errors2[s][n][6]]
	data=[errorObs2[s][n]] #,errorHH2[s][n]
	for i in order:
		data.append(errors2[s][n][i])
	if len(scalesN)==1:
		axes[1].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		axes[1].axhline(y=sum(data[0])/len(data[0]),color="black", linestyle="--")
		set_axis_style(axes[1],x)

	else:
		axes[1][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1][s].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		axes[1][s].axhline(y=sum(data[0])/len(data[0]),color="black", linestyle="--")
		set_axis_style(axes[1][s],x)
fig.tight_layout()
#fig.suptitle("Error in estimation of equilibrium frequencies")
fig.subplots_adjust(hspace=0.3)
plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_errors_fewerMethods_new2")


if not len(scalesN)==1:
	fs = 10  # fontsize
	if args.AA:
		pos=[1,2,3,4,5]
		x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$"]
		order=[0,5,2,4]
	else:
		if nHum>=10000:
			#methods=[12,13]
			pos=[1,2,3,4]
			x = ["Observed", "HH94", "GSC94", "$\overline{w}_s$"]
			order=[0,2,1]
		elif nHum>1:
			#methods=[9,12,13]
			pos=[1,2,3,4,5]
			x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$"]
			order=[0,3,1,2]
		else:
			#methods=[14,8,9,11,12,13,6]
			pos=[1,2,3,4,5,6]
			x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$","PhyML"]
			order=[0,6,3,5,7]
	fig, axes = plt.subplots(nrows=2, ncols=len(scalesN), figsize=(len(scalesN)*12, 10))
	n=0
	for s in range(len(scalesN)):
		#for n in range(len(nLeaves)):
		#data = [errorObs1[s][n],errorObs2[s][n],errors1[s][n][0],errors2[s][n][0],errorsB1[s][n][0],errorsB2[s][n][0],errors1[s][n][1],errors2[s][n][1],errorsB1[s][n][1],errorsB2[s][n][1],errors1[s][n][2],errors2[s][n][2],errors1[s][n][3],errors2[s][n][3]]#
		#data = [entropiesObs1[s][n],entropiesHH1[s][n],entropies1[s][n][5],entropies1[s][n][2],entropies1[s][n][4],entropies1[s][n][6]]#
		data=[entropiesObs1[s][n]] #,entropiesHH1[s][n]
		for i in order:
			data.append(entropies1[s][n][i])
		axes[0][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0][s].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		axes[0][s].axhline(y=0, xmin=0, xmax=1, linestyle="dashed")
		set_axis_style(axes[0][s],x)
		
		#data = [entropiesObs2[s][n],entropiesHH2[s][n],entropies2[s][n][5],entropies2[s][n][2],entropies2[s][n][4],entropies2[s][n][6]]
		data=[entropiesObs2[s][n]] #,entropiesHH2[s][n]
		for i in order:
			data.append(entropies2[s][n][i])
		axes[1][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1][s].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		axes[1][s].axhline(y=0, xmin=0, xmax=1, linestyle="dashed")
		set_axis_style(axes[1][s],x)
	fig.tight_layout()
	#fig.suptitle("Error in estimation of equilibrium frequencies")
	fig.subplots_adjust(hspace=0.3)
	plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_consScores_fewerMethods_new2")



# fs = 10  # fontsize
# if args.AA:
# 	pos=[1,2,3,4,5]
# 	x = ["Obs", "HH94", "GSC94", "PNS", "fastPNS"]
# 	order=[5,2,4]
# else:
# 	pos=[1,2,3,4,5,6]
# 	x = ["Obs", "HH94", "GSC94", "PNS", "fastPNS","PhyML"]
# 	order=[5,2,4,6]
# fig, axes = plt.subplots(nrows=2, ncols=len(scalesN), figsize=(len(scalesN)*12, 10))
# n=0
# for s in range(len(scalesN)):
# 	#for n in range(len(nLeaves)):
# 	#data = [errorObs1[s][n],errorObs2[s][n],errors1[s][n][0],errors2[s][n][0],errorsB1[s][n][0],errorsB2[s][n][0],errors1[s][n][1],errors2[s][n][1],errorsB1[s][n][1],errorsB2[s][n][1],errors1[s][n][2],errors2[s][n][2],errors1[s][n][3],errors2[s][n][3]]#
# 	data = [entropiesObs1[s][n],entropiesHH1[s][n],entropies1[s][n][5],entropies1[s][n][2],entropies1[s][n][4],entropies1[s][n][6]]#
# 	axes[0][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
# 	axes[0][s].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
# 	axes[0][s].axhline(y=0, xmin=0, xmax=1, linestyle="dashed")
# 	set_axis_style(axes[0][s],x)
# 		
# 	data = [entropiesObs2[s][n],entropiesHH2[s][n],entropies2[s][n][5],entropies2[s][n][2],entropies2[s][n][4],entropies2[s][n][6]]
# 	data=[entropiesObs2[s][n],entropiesHH2[s][n]]
# 		for i in order:
# 			data.append(entropies2[s][n][i])
# 	axes[1][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
# 	axes[1][s].set_title('scale '+scalesNnew[s]+" alpha 0.1", fontsize=fs)
# 	axes[1][s].axhline(y=0, xmin=0, xmax=1, linestyle="dashed")
# 	set_axis_style(axes[1][s],x)
# fig.tight_layout()
# #fig.suptitle("Error in estimation of equilibrium frequencies")
# fig.subplots_adjust(hspace=0.3)
# plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_consScores_fewerMethods")





	fs = 10  # fontsize
	if args.AA:
		pos=[1,2,3,4,5]
		x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$"]
		order=[0,5,2,4]
	else:
		if nHum>=10000:
			#methods=[12,13]
			pos=[1,2,3,4]
			x = ["Observed", "HH94", "GSC94", "$\overline{w}_s$"]
			order=[0,2,1]
		elif nHum>1:
			#methods=[9,12,13]
			pos=[1,2,3,4,5]
			x = ["Observed", "HH94", "GSC94", "$w_S$", "$\overline{w}_s$"]
			order=[0,3,1,2]
		else:
			#methods=[14,8,9,11,12,13,6]
			pos=[1,2,3,4,5,6]
			x = ["Observed", "HH94", "GSC94", "$w_s$", "$\overline{w}_s$","PhyML"]
			order=[0,6,3,5,7]
	fig, axes = plt.subplots(nrows=2, ncols=len(scalesN), figsize=(len(scalesN)*12, 10))
	n=0
	for s in range(len(scalesN)):
		#for n in range(len(nLeaves)):
		#data = [errorObs1[s][n],errorObs2[s][n],errors1[s][n][0],errors2[s][n][0],errorsB1[s][n][0],errorsB2[s][n][0],errors1[s][n][1],errors2[s][n][1],errorsB1[s][n][1],errorsB2[s][n][1],errors1[s][n][2],errors2[s][n][2],errors1[s][n][3],errors2[s][n][3]]#
		#data = [entropiesErrObs1[s][n],entropiesErrHH1[s][n],entropiesErr1[s][n][5],entropiesErr1[s][n][2],entropiesErr1[s][n][4],entropiesErr1[s][n][6]]#
		data=[entropiesErrObs1[s][n]] #,entropiesErrHH1[s][n]
		for i in order:
			data.append(entropiesErr1[s][n][i])
		axes[0][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[0][s].set_title('scale '+scalesNnew[s]+" background", fontsize=fs)
		set_axis_style(axes[0][s],x)
		
		#data = [entropiesErrObs2[s][n],entropiesErrHH2[s][n],entropiesErr2[s][n][5],entropiesErr2[s][n][2],entropiesErr2[s][n][4],entropiesErr2[s][n][6]]
		data=[entropiesErrObs2[s][n]] #,entropiesErrHH2[s][n]
		for i in order:
			data.append(entropiesErr2[s][n][i])
		axes[1][s].violinplot(data, pos, points=20, widths=0.8, showmeans=True, showextrema=True, showmedians=False)
		axes[1][s].set_title('scale '+scalesNnew[s]+" alpha "+str(diri), fontsize=fs)
		set_axis_style(axes[1][s],x)
	fig.tight_layout()
	#fig.suptitle("Error in estimation of equilibrium frequencies")
	fig.subplots_adjust(hspace=0.3)
	plt.savefig(filePath+str(diriS)+"_"+str(nReps)+"reps_consScoresErr_fewerMethods_new2")









exit()
			

